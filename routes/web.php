<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::group(['as'=>'pedidos::'], function(){

    Route::get('/pedidos', ['as' => 'index', 'uses' => 'PedidosController@index']);
    Route::get('/pedidos/datosClientes/{id?}', ['as' => 'datosClientes', 'uses' => 'PedidosController@datosClientes']);
    Route::post('/pedidos/savePedido', 'PedidosController@savePedido');
    Route::get('/pedidos/getPedidos', 'PedidosController@getPedidos');
    Route::get('/pedidos/obtenerProductos/{id?}', ['as' => 'obtenerProductos', 'uses' => 'PedidosController@obtenerProductos']);
    Route::get('/pedidos/delete/{id?}', ['as' => 'delete', 'uses' => 'PedidosController@delete']);
    Route::get('/pedidos/searchProducto/{id?}', ['as' => 'searchProducto', 'uses' => 'PedidosController@searchProducto']);

});

Route::get('/productos', ['as' => 'index', 'uses' => 'ProductosController@index']);
Route::post('/productos/saveImagenProducto','ProductosController@saveImagenProducto');


Route::group(['as'=>'clientes::'], function(){
    Route::get('/clientes', ['as' => 'index', 'uses' => 'ClientesController@index']);
    Route::get('/clientes/getClientes/{id?}', ['as' => 'getClientes', 'uses' => 'ClientesController@getClientes']);
    Route::post('/clientes/saveClientes', 'ClientesController@saveClientes');
    Route::put('/clientes/updateClientes', 'ClientesController@updateClientes');
    Route::get('/clientes/getConfiguracion', ['as' => 'getConfiguracion', 'uses' => 'ClientesController@getConfiguracion']);
    Route::post('/clientes/delete', 'ClientesController@delete');
});

Route::group(['as'=>'zonas::'], function(){
    Route::get('/zonas/getComboZona/{id?}', ['as' => 'getComboZona', 'uses' => 'ZonasController@getComboZona']);
});



Route::group(['as'=>'configuracion::'], function(){

    Route::get('/configuracion', ['as' => 'index', 'uses' => 'ConfiguracionController@index']);
    Route::get('/configuracion/getClientes/{id?}', ['as' => 'getclientes', 'uses' => 'ConfiguracionController@getclientes']);
    Route::post('/configuracion/save', ['as' => 'save', 'uses' => 'ConfiguracionController@save']);
    Route::get('/configuracion/delete/{id?}', ['as' => 'delete', 'uses' => 'ConfiguracionController@delete']);
});


Route::get('/home', 'HomeController@index')->name('home');

Route::group(['as'=>'usuarios::'], function(){
    Route::get('/usuarios', ['as' => 'index', 'uses' => 'UsuariosController@index']);
    Route::get('/usuarios/get/{idUsuario?}', ['as' => 'get', 'uses' => 'UsuariosController@get']);
    Route::post('/usuarios/save', ['as' => 'save', 'uses' => 'UsuariosController@save']);
    Route::get('/usuarios/delete/{id?}', ['as' => 'delete', 'uses' => 'UsuariosController@delete']);
    Route::get('/usuarios/password', ['as' => 'password', 'uses' => 'UsuariosController@password']);
    Route::get('/usuarios/getComboUsuarios', ['as' => 'getComboUsuarios', 'uses' => 'UsuariosController@getComboUsuarios']);
    Route::post('/usuarios/updatepassword', ['as' => 'updatepassword', 'uses' => 'UsuariosController@updatepassword']);

});

Route::group(['as'=>'roles::'], function(){
    Route::get('/roles', ['as' => 'index', 'uses' => 'RolesController@index']);
    Route::get('/roles/getComboRoles', ['as' => 'getComboRoles', 'uses' => 'RolesController@getComboRoles']);
    Route::get('/roles/get/{idRol?}', ['as' => 'get', 'uses' => 'RolesController@get']);
    Route::post('/roles/save', ['as' => 'save', 'uses' => 'RolesController@save']);
    Route::get('/roles/delete/{id?}', ['as' => 'delete', 'uses' => 'RolesController@delete']);
    Route::get('/roles/getPermisos/{idRol?}', ['as' => 'getPermisos', 'uses' => 'RolesController@getPermisos']);
    Route::post('/roles/AddPermission', ['as' => 'AddPermission', 'uses' => 'RolesController@AddPermission']);
    Route::post('/roles/revokePermission', ['as' => 'revokePermission', 'uses' => 'RolesController@revokePermission']);
});

Route::group(['as'=>'permisos::'], function(){
    Route::get('/permisos', ['as' => 'index', 'uses' => 'PermisosController@index']);
    Route::get('/permisos/getForCombo', ['as' => 'getForCombo', 'uses' => 'PermisosController@getForCombo']);
    Route::get('/permisos/get/{idRol?}', ['as' => 'get', 'uses' => 'PermisosController@get']);
    Route::post('/permisos/save', ['as' => 'save', 'uses' => 'PermisosController@save']);
    Route::get('/permisos/delete/{id?}', ['as' => 'delete', 'uses' => 'PermisosController@delete']);
});

Route::group(['as'=>'post::'], function(){
     Route::get('/post', ['as' => 'index', 'uses' => 'PostsController@index']);
     Route::get('/post/{id?}', [ 'as' => 'show', 'uses' => 'PostsController@show']);
});



Route::get('/prueba', 'Controller@obtenerAccessToken');
 // Route::post('/prueba', ['as' => 'prueba', 'uses' => 'Controller@obtenerAccessToken']);

