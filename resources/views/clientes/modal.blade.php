<!--Modal para la modificar la contraseña de un usuario-->
<div id="mCRUD" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-focus-on="input:first" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button style="color:white;" type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Nuevo Cliente</h4>
            </div>

            <div class="modal-body" >
                <form action="#" id="formCRUD" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">

                        <div class="row hide">
                            <div class="col-md-12">
                                <input type="text" id="id" name="id_cliente" class="form-control">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Rif:</label>
                                    <input type="text" id="rif" name="rif" class="form-control" required MaxLength="10">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Razón Social:</label>
                                    <input type="text" id="razon_social" name="razon_social" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="text" id="email" name="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Persona Contacto:</label>
                                    <input type="text" id="contacto" name="contacto" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Zona:</label>
                                    <select type="text" id="zona_id" name="id_zona" class="form-control select_2"  required style="width: 100%" >
                                        @foreach($zonas as $zona)
                                        <option value="{{$zona->id_zona}}">{{$zona->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Tipo Cliente:</label>
                                    <select type="text" id="tipo_cliente_id" name="id_tipo_cliente" class="form-control select_2"  required style="width: 100%" >
                                        <option></option>
                                        @foreach($tipocliente as $tipocl)
                                        <option value="{{$tipocl->id_tipo_cliente}}">{{$tipocl->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Segmento:</label>
                                    <select type="text" id="segmento_id" name="id_segmento" class="form-control select_2"  required style="width: 100%" >
                                        <option></option>
                                        @foreach($segmentos as $segmento)
                                        <option value="{{$segmento->id_segmento}}">{{$segmento->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Vendedor:</label>
                                    <select type="text" id="vendedor_id" name="id_vendedor" class="form-control select_2"  required style="width: 100%" >
                                        @foreach($vendedores as $vendedor)
                                        <option value="{{$vendedor->id_vendedor}}">{{$vendedor->nombre_completo}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Lista de Precios:</label>
                                    <select type="text" id="listaprecio_id" name="id_listaprecio" class="form-control select_2"  required style="width: 100%" >
                                        <option></option>
                                        @foreach($listaprecio as $lista)
                                        <option value="{{$lista->id_listaprecio}}">{{$lista->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Condición de Pago:</label>
                                    <select type="text" id="condicion_de_pago_id" name="id_condicion_de_pago" class="form-control select_2"  required style="width: 100%" >
                                        <option></option>
                                        @foreach($condiciondepago as $condicion)
                                        <option value="{{$condicion->id_condicion_de_pago}}">{{$condicion->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Porcentaje Descuento:</label>
                                    <input type="text" id="porcentaje" name="porce_descuento_global" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Límite Crédito:</label>
                                    <input type="text" id="credito" name="limite_credito" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Dirección Fiscal:</label>
                                    <input type="text" id="direccion_fiscal" name="direccion_fiscal" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Dirección de Entrega:</label>
                                    <input type="text" id="direccion_entrega" name="direccion_entrega" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Teléfonos:</label>
                                    <input type="text" id="telefono" name="telefono" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Status:</label>
                                    <select type="text" id="status_id" name="status" class="form-control select_2" required style="width: 100%">
                                        <option value="0">Activo</option>
                                        <option value="1">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="javascript:;" id="btCancelar" data-dismiss="modal" class="btn green "> Cancelar
                                <i class="fa fa-close"></i>
                            </a>
                            <a href="javascript:;" id="btGuardar" class="btn btn-success"> Guardar
                                <i class="fa fa-check"></i>
                            </a>
                            <a href="javascript:;" id="btActualizar" class="btn btn-success hide"> Actualizar
                                <i class="fa fa-check"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>
<style>

    [aria-labelledby="select2-IdIVA-container"], [aria-labelledby="select2-idTipoDoc-container"] {

    }
</style>