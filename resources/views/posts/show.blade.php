@extends('layouts.app') 
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Blog</span>
                    <span class="caption-helper">Consumiendo API</span>
                </div>
            </div>
<body>
	<div class="container">
		<h1>Publicaciones</h1>

		<div class="panel panel-default">

			<div class="panel-heading">
				<h4>{{ $post->title }}</h4>
			</div>

			<div class="panel-body">

				{{ $post->body }}
				
			</div>


		</div>

		<input class="btn btn-warning" type="button" value="Regresar" onclick="history.back(-1)" />
			
	</div>
	
</body>
        </div>

    </div>
</div>

@endsection





<!--
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="/css/app.css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<title>Blog</title>
</head>
<body>
	<div class="container">
		<h1>Publicaciones</h1>

		<div class="panel panel-default">

			<div class="panel-heading">
				<h4>{{ $post->title }}</h4>
			</div>

			<div class="panel-body">

				{{ $post->body }}
				
			</div>


		</div>

		<a href="/" class="btn btn-warning">Regresar</a>
			
	</div>
	
</body>
</html>