@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Configuración de Usuarios</span>
                    <span class="caption-helper">Visualizar y administrar</span>
                </div>

                <div class="actions">

                   <!--  <button class="btn green btn-outline btn-circle modal-toggle btn" data-target="#mCRUD" data-toggle="modal" id="btnAgregar"><i class="fa fa-plus"></i> Registrar Clientes </button> -->

                    <div class="btn-group">
                        <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-share"></i>
                            <span class="hidden-xs">Herramientas</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right" id="table_tools">

                            <li class="hidden">
                                <a href="javascript:;" data-action="0" class="tool-action">
                            <i class="icon-printer"></i> Print</a>
                            </li>
                            <li class="hidden">
                                <a href="javascript:;" data-action="1" class="tool-action">
                            <i class="icon-check"></i> Copy</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="2" class="tool-action">
                            <i class="icon-doc"></i> PDF</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="3" class="tool-action">
                            <i class="icon-paper-clip"></i> Excel</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="4" class="tool-action">
                            <i class="icon-cloud-upload"></i> CSV</a>
                            </li>
                            <!--<li>
                                <a href="javascript:;" data-action="5" class="tool-action">
                                    <i class="icon-cloud-upload"></i> Campos</a>
                            </li>-->
                            <li class="divider"> </li>
                            <li>
                                <a href="javascript:;" data-action="6" class="tool-action">
                                <i class="icon-refresh"></i> Reload</a>
                            </li>

                        </ul>
                    </div>
                </div>


            </div>
            <div class="portlet-body">
            <form action="#" id="formCRUD" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">

                        <div class="row hide">
                            <div class="col-md-12">
                                <input type="text" id="id" name="id_cliente" class="form-control">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8 col-xs-12">
                                <div class="form-group">
                                    <label>Usuarios:</label>
                                    <select type="text" id="usuario_id" name="user_id" class="form-control select_2" required >
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Vendedor:</label>
                                        <select type="select" id="vendedor" name="id_vendedor" class="form-control select_2"  required style="width: 100%" >
                                                <option></option>
                                                @foreach($vendedores as $vendedor)
                                                <option value="{{$vendedor->id_vendedor}}">{{$vendedor->nombre_completo}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Zona:</label>
                                    <select type="select" id="zona_id" name="id_zona" class="form-control select_2"  required style="width: 100%" >
                                        <option></option>
                                        @foreach($zonas as $zona)
                                        <option value="{{$zona->id_zona}}">{{$zona->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tipo Cliente:</label>
                                    <select type="text" id="tipo_cliente_id" name="id_tipo_cliente" class="form-control select_2"  required style="width: 100%" >
                                        <option></option>
                                        @foreach($tipocliente as $tipocl)
                                        <option value="{{$tipocl->id_tipo_cliente}}">{{$tipocl->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Segmento:</label>
                                    <select type="text" id="segmento_id" name="id_segmento" class="form-control select_2"  required style="width: 100%" >
                                        <option></option>
                                        @foreach($segmentos as $segmento)
                                        <option value="{{$segmento->id_segmento}}">{{$segmento->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>                                             
                    </div>
                    <div class="margin-top-10">
                            <button type="button" id="btnConfigUser" class="btn btn-success">Guardar</button>
                            <a href="javascript:;" class="btn default"> Cancelar </a>
                        </div>
                </form>
            </div>
        </div>

    </div>
</div>

@include('clientes.modal')


@endsection

