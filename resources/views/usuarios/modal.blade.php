<!--Modal para la modificar la contraseña de un usuario-->
<div id="mCRUD" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-focus-on="input:first">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Usuario</h4>
            </div>

            <div class="modal-body" >
                <form action="#" id="formCRUD" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">

                        <div class="row hide">
                            <div class="col-md-12">
                                <input type="text" id="id" name="id" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('Nombres') }}:</label>
                                    <input type="text" id="nombres" name="nombres" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('Apellidos') }}:</label>
                                    <input type="text" id="apellidos" name="apellidos" class="form-control" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('Email') }}</label>
                                    <input type="email" id="email" name="email" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('Password') }}:</label>
                                    <input type="pass" id="password" name="password" class="form-control">
                                </div>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('Rol') }}</label>
                                    <select type="select" id="role_id" name="role_id" class="form-control select_2" title="Seleccione..." style="width:100%" required>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('Teléfono') }}</label>
                                    <input type="text" id="telefono" name="telefono" class="form-control" required>
                                </div>
                            </div>
                        </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                             <label>{{ __('Vendedor') }}:</label>
                                 <select type="text" class="form-control select_2" id="vendedor_id"  name="vendedor_id" style="width:100%"  required>
                                        @foreach($vendedores as $vendedor)
                                        <option value="{{$vendedor->id_vendedor}}">{{$vendedor->nombre_completo}}</option>
                                        @endforeach
                                </select>
                             </div>
                         </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Activo:</label>
                                <select type="select" id="Hab" name="Hab" class="form-control select_2" title="Seleccione..." style="width:100%">
                                    <option value="1" selected>Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="javascript:;" id="btCancelar" class="btn green "> Cancelar
                                <i class="fa fa-close"></i>
                            </a>
                            <a href="javascript:;" id="btGuardar" class="btn green "> Guardar
                                <i class="fa fa-check"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>
<style>

    [aria-labelledby="select2-id_rol-container"] {
        background-color: #a2ffc4 !important;
    }
</style>