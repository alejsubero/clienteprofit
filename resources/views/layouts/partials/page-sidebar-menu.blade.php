
<div class="page-sidebar navbar-collapse collapse">

    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu page-sidebar-menu-closed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
    

        @if(Auth::user()->role_id == 1)
            @include ('layouts.partials.menu.admin')
        @else
            @include ('layouts.partials.menu.standard')
        @endif

      <!--   {!! session()->get('menu') !!} -->


    </ul>
    <!-- END SIDEBAR MENU -->
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->
