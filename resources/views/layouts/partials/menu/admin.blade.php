
        <li class="nav-item ">
            <a href="{{ url('/home') }}" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span  style="text-transform: uppercase" class="title">Home</span>
               <!--  <span class="selected"></span>
               -->
            </a>
        </li>
        <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span style="text-transform: uppercase" class="title">Admininistrar Usuarios</span>
                  <!--   <span class="selected"></span> -->
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ url('/usuarios') }}" class="nav-link nav-toggle">
                            <i class="fa fa-user"></i>
                            <span class="title">Usuarios</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/roles') }}" class="nav-link nav-toggle">
                            <i class="fa fa-filter"></i>
                            <span class="title">Roles de Usuarios</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                </ul>
            </li>
 
            <li class="nav-item">
                <a href="{{ url('/clientes') }}" class="nav-link nav-toggle">
                    <i class="fa fa-cubes"></i>
                    <span  style="text-transform: uppercase" class="title">Clientes</span>
                    <!--   <span class="selected"></span> -->
                    <span class="arrow open"></span>
                </a>
            </li>
            <li class="nav-item">
                <a id="pedido" href="{{ url('/pedidos') }}" class="nav-link nav-toggle">
                    <i class="fa fa-briefcase"></i>
                    <span  style="text-transform: uppercase" class="title">Pedidos</span>
                    <!-- <span class="selected"></span> -->
                    <span class="arrow open"></span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url('/productos') }}" class="nav-link nav-toggle">
                    <i class="fa fa-folder-open"></i>
                    <span  style="text-transform: uppercase" class="title">Productos</span>
                    <!-- <span class="selected"></span> -->
                    <span class="arrow open"></span>
                </a>
            </li>
            <li class="nav-item">
                <a href="3" class="nav-link nav-toggle">
                    <i class="fa fa-sitemap"></i>
                    <span  style="text-transform: uppercase" class="title">Ventas</span>
                    <!-- <span class="selected"></span> -->
                    <span class="arrow open"></span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url('/configuracion') }}" class="nav-link nav-toggle">
                    <i class="fa fa-cogs"></i>
                    <span  style="text-transform: uppercase" class="title">Mantenimiento</span>
                    <!-- <span class="selected"></span> -->
                    <span class="arrow open"></span>
                </a>
            </li>
        
