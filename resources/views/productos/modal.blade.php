<!--Modal para la modificar la contraseña de un usuario-->
<div id="mCRUD" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-focus-on="input:first" data-keyboard="false">
    <div class="modal-dialog modal-lg" id="tamañomodalpedidos">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Nuevo Pedido</h4>
            </div>

            <div class="modal-body" >
                <form action="#" id="formCRUDPedidos" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-body">
                        <div class="row hide">
                            <div class="col-md-12">
                                <input type="text" id="id" name="id" class="form-control">

                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="portlet light bordered">
                                        <div class="portlet-title">
                                                <div class="caption font-dark">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject bold uppercase">Datos del Cliente</span>
                
                                                </div>
                                            </div>
                                            <div class="row ">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Clientes</label>
                                                        <select type="text" id="cliente_id" name="id_cliente" class="form-control select_2"  required style="width: 100%" >
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Rif</label>
                                                        <input type="text" id="rif" name="rif" class="form-control"  required>
                                                    </div>
                                                </div>
                                     
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Dirección Fiscal</label>
                                                        <input type="text" id="direccion_fiscal" name="direccion_fiscal" class="form-control"  required>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Dirección de Entrega</label>
                                                        <input type="text" id="direccion_entrega" name="direccion_entrega" class="form-control"  required>
                                                    </div>
                                                </div>
                                            </div>
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Seleccionar Productos</span>
        
                                        </div>
                                    </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                             <table class="table table-striped table-bordered table-hover order-column" id="dtTablePedidos" width="100%">
                                                <thead>
        
                                                </thead>
                                            </table> 
                                        </div>
                                    </div>
                                </div>
                             </div>
                            </div>
                        </div>		        
                    </div>
                </form>
               
  
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="javascript:;" id="btCancelar" data-dismiss="modal" class="btn green "> Finalizar
                                <i class="fa fa-close"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>
<style>
    #mCRUD .modal-body{
      padding: 5px !important;
    }
    #tamañomodalpedidos{
      width: 95% !important;
    }

    .add{
        float: right;
    }
</style>

<!-- 
    
<script>
        $(document).ready(function() {
    var table = $('#example').DataTable();
 
    $('button').click( function() {
        var data = table.$('input, select').serialize();
        alert(
            "The following data would have been submitted to the server: \n\n"+
            data.substr( 0, 120 )+'...'
        );
        return false;
    } );
} );

</script> -->
