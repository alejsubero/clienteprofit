@extends('layouts.app')
@section('content')

<div class="row ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Pedidos</span>
                    <span class="caption-helper">Visualizar y administrar Pedidos</span>
                </div>

                <div class="actions">
                    <div class="btn-group">
                        <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-share"></i>
                            <span class="hidden-xs">Herramientas</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right" id="table_tools">

                            <li class="hidden">
                                <a href="javascript:;" data-action="0" class="tool-action">
                            <i class="icon-printer"></i> Print</a>
                            </li>
                            <li class="hidden">
                                <a href="javascript:;" data-action="1" class="tool-action">
                            <i class="icon-check"></i> Copy</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="2" class="tool-action">
                            <i class="icon-doc"></i> PDF</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="3" class="tool-action">
                            <i class="icon-paper-clip"></i> Excel</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="4" class="tool-action">
                            <i class="icon-cloud-upload"></i> CSV</a>
                            </li>
                            <!--<li>
                                <a href="javascript:;" data-action="5" class="tool-action">
                                    <i class="icon-cloud-upload"></i> Campos</a>
                            </li>-->
                            <li class="divider"> </li>
                            <li>
                                <a href="javascript:;" data-action="6" class="tool-action">
                                <i class="icon-refresh"></i> Reload</a>
                            </li>

                        </ul>
                    </div>
                </div>


            </div>

            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="" id="addpedido" class="btn btn-success Element" data-target="#mCRUD" data-toggle="modal" > Agregar Pedido
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tablasecundaria" class="portlet-body">
                <table class="table table-striped table-condensed flip-content table-bordered table-hover order-column" id="dtTableP" width="100%">
                    <thead>
                    </thead>
                </table>
            </div>
            <div id="tablePrincipal" class="portlet-body">
                <table class="table table-striped table-condensed flip-content table-bordered table-hover order-column" id="dtTable" width="100%">
                    <thead>
                    </thead>
                </table>
            </div>
            <div class="row">
                <a style="margin-right:2%;" href="javascript:;" id="btGuardarPedido" class="btn btn green pull-right hide"> Guardar
                    <i class="fa fa-check"></i>
                </a>
            </div>
           
        </div>

    </div>
</div>

@include('productos.modal')


@endsection

