<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    protected $table = 'pedidos';

    protected $fillable = ['idproducto','descripcion','precio','cantidad','iva','tot'];
}
