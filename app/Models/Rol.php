<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'roles';
    
    protected $fillable = [
        'name', 'slug', 'description', 'created_at', 'updated_at', 'special'
    ];

    protected $hidden = [
        
    ];
}
