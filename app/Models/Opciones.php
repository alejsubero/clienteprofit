<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Opciones extends Model
{
    protected $table = 'con_opciones';

    protected $fillable = [
        'id','idOpcion', 'Descrip', 'Parent', 'Nivel', 'Orden', 'Imagen', 'Hab', 'Controlador', 'Accion', 'EsLink', 'Metodo', 'permission_id'
    ];

    protected $hidden = [
        'Clave', 'remember_token', 'ClaveEnvio', 'ClaveFacebook',
    ];

}