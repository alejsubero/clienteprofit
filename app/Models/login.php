<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{

    protected $table = 'con_login';
    
    protected $fillable = [
        'id', 'id_broker', 'id_usuario', 'fecha_login', 'fecha_logout', 'id_session', 'created_at', 'updated_at', 'activa'
    ];

    protected $hidden = [
        
    ];
}