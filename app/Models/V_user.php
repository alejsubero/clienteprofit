<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class V_user extends Model
{
      protected $table = "vistauser";

    protected $fillable = [
                            'id', 
                            'nombres', 
                            'apellidos', 
                            'telefono', 
                            'facebook', 
                            'color', 
                            'Hab', 
                            'role_id',
                            'vendedor_id',
                            'ultima_sesion',
                            'created_at', 
                            'updated_at'
                          ];



   
}
