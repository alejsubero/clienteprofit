<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
    protected $table = 'config_users';

    protected $fillable = ['id','user_id','id_zona','id_tipo_cliente','id_segmento','id_vendedor'];
}
