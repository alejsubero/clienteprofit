<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'v_roles_con_opciones';

    protected $fillable = [
        'id', 'idOpcion', 'Descrip', 'Parent', 'Nivel', 'Orden', 'Imagen', 'Hab', 'Ruta', 'EsLink', 'Metodo', 'role_id', 'permission_id',
    ];

    protected $hidden = [
    ];

}