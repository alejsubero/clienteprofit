<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;


class PermisosController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->middleware('auth');

        Config::set('app.module', 'Permisos');
    }

    public function index(){
        
        return view('permisos.index');
    }

    public function get(Request $request, $idPermiso = null){

        $permisos = [];
        $response = [];

        if($idPermiso == null){
            $start = $request->input('start');
            $length = $request->input('length');
            $search = $request->input('search')['value'];
            $draw = (int)$request->input('draw');

            /*Para el ordenamiento*/
            $orderCol = $request->input('order.0.column');
            $orderDir = $request->input('order.0.dir');
            if($orderCol == null){
                $orderCol = 0;
            }
            if($orderDir == null){
                $orderDir = 'asc';
            }
            /**/

            $columns = ['id', 'name', 'slug', 'description'];
            
            if($search != null){
                
                //$permisos = Permission::all();

                $permisos = Permission::where('name', 'like', '%' . $search . '%')
                ->orWhere('slug', 'like', '%' . $search . '%')
                ->orWhere('description', 'like', '%' . $search . '%');
                
                $permisos = $permisos->orderBy($columns[$orderCol], $orderDir)->skip($start)->take($length)->get($columns);
            }else{

                $permisos = Permission::orderBy($columns[$orderCol], $orderDir)->skip($start)->take($length)->get($columns);

            }

            $recordsTotal = Permission::count();
            $recordsFiltered = $permisos->count();

            $response = ['data' => $permisos, 'recordsFiltered' => $recordsFiltered, 'recordsTotal' => $recordsTotal, 'draw' => $draw, '$search' => ''];
        }else{
            $permisos = Permission::where('id', $idPermiso)->get();
            $response = ['data' => $permisos];
        }
       
        return response()->json($response);
    }

    /*public function getForCombo(){
        
        $TipoDocumento = Role::all(['id AS val', 'name AS text']);
        $response = ['list' => $TipoDocumento];

        return response()->json($response);
    }*/

    public function save(){

        $permiso = Permission::firstOrNew(array('id' => Input::get('id')));

        $permiso->name = Input::get("name");
        $permiso->slug = Input::get("slug");
        $permiso->description = Input::get("description");

        if($permiso->save()) {
            $response = ['data' => $permiso, 'success' => true, 'id' => $permiso->id, 'messaje' => 'Permiso creado correctamente.'];
        } else {
            $response = ['data' => [], 'success' => false, 'id' => -1, 'messaje' => 'Error al crear el Permiso.'];
        }

        return response()->json($response);

    }

    public function delete($id = null){
        
        $permiso = Permission::find($id);

        if($permiso->delete()) {
            $response = ['data' => $permiso, 'success' => true, 'id' => $rol->id, 'messaje' => 'Permiso eliminado.'];
        } else {
            $response = ['data' => [], 'success' => false, 'id' => -1, 'messaje' => 'Error al eliminar el Permiso.'];
        }

        return response()->json($response);
    }
}
