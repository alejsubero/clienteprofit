<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ZonasController extends Controller
{
    public function getComboZona()
    {
        $zonas = $this->realizarPeticion('GET', 'http://apiprofit.test/zonas', ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ],]);

       // $response = ['list' => $designations];

        return view('clientes.index', compact('zonas'));
    }
}
