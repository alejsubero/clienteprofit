<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Image;

class ProductosController extends Controller
{
    public function index()
    {
       
        $productos = $this->getProductos();
       
    	return view('productos.index',  compact('productos'));

    }

    public function saveImagenProducto(Request $request)
    {       
        $id = $request->input('id_productofoto');
        $id_producto = $request->input('id_producto');
        $orden = $request->input('orden');
        //$foto = json_decode($request->all(),true);
        //$image_name = "foto". "." . "jpg";
       //dd($foto);
        $total = [
            'id_productofoto' =>   $id,
            'id_producto' =>  $id_producto,
            'foto'  => "hola",
            'orden' =>   $orden
        ] ;
     //dd($total);

        $accessToken = 'Bearer ' . $this->obtenerAccessToken();
     
        $respuesta = $this->realizarPeticion('POST', 'http://apiprofit_new.test/productofotos',
         ['headers' =>['Authorization' => $accessToken],
          [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ], 'form_params' => $total]);
      
        return response()->json($respuesta);
    }


}
