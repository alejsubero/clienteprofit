<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

use App\Models\User as Usuario;
use App\Models\V_user;

class UsuariosController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth');
        Config::set('app.module', 'Usuarios');
    }

    public function index()
    {
        $vendedores       = $this->getVendedores();
        return view('usuarios.index', compact('vendedores'));
    }

    public function get(Request $request)
    {
        $usuarios = [];
        if (Input::has('id')) {
            $id                 = Input::get('id');
            $usuarios           = V_user::where('id', $id)->get();
        }else{
            $usuarios           = V_user::all();

        }
        $response = ['list' => $usuarios];
        return response()->json($response);


    }

    public function save(){


        $usuario = Usuario::firstOrNew(array('id' => Input::get('id')));

        $usuario->nombres = Input::get('nombres');
        $usuario->apellidos = Input::get('apellidos');
        $usuario->password = Hash::make(Input::get('password'));
        $usuario->email = Input::get('email');
        $usuario->telefono = Input::get('telefono');
        $usuario->facebook = Input::get('facebook');
        $usuario->Hab = Input::get('Hab');
        $usuario->role_id = (int)Input::get('role_id');
        $usuario->vendedor_id = Input::get('vendedor_id');

        $user = null;
        if($usuario->save()) {

            $usuario->revokeAllRoles();
            $usuario->assignRole($usuario->role_id);

            $response = ['data' => $usuario, 'success' => true, 'id' => $usuario->id, 'messaje' => 'Usuario guardado.', '$user' => $usuario->id];
        } else {
            $response = ['data' => [], 'success' => false, 'id' => -1, 'messaje' => 'Error al guardar.'];
        }

        return response()->json($response);

    }

    public function delete($id = null)
    {

        $usuario = Usuario::find($id);

        if($usuario->delete()) {
            $response = ['data' => $usuario, 'success' => true, 'id' => $usuario->id, 'messaje' => 'Usuario eliminado.'];
        } else {
            $response = ['data' => [], 'success' => false, 'id' => -1, 'messaje' => 'Usuario no Eliminado.'];
        }

        return response()->json($response);
    }

        public function password()
    {
        return view('usuarios.password');
    }

    public function updatepassword(Request $request)
    {

     $user = Auth::user();
     $user->password = Hash::make($request->get('password'));


     if ($user->save()) {

         $response = ['data' => $user, 'success' => true, 'id' => $user->password, 'messaje' => 'contraseña Actualizada correctamente.'];
     }


        return response()->json($response);

   // }

    }

    public function getComboUsuarios(){
        
        $usuarios = V_user::all(['id AS val', 'nombres AS text','apellidos AS text2']);
        $response = ['list' => $usuarios];
      
        return response()->json($response);
    }

}
