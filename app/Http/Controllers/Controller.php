<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Support\Facades\Route;
use App\Models\Opciones;
use App\Models\Roles_con_opciones;
use App\Models\Menu;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
//use GuzzleHttp\Psr7\Request;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {

        if (Auth::check()){
            $this->menu();
        }

    }

    public function menu(){

        $opciones = Menu::where('role_id', Auth::user()->role_id)->get();

        $m = $this->armarMenuHTML($opciones, $opciones->where('Parent', 0), false);
        session()->put('menu', $m);
    }

    public function armarMenuHTML($todo, $subConjunto, $hijo){

        $menuHTML = "";
        if ($hijo == false)
        { }
        else
        {  }

        foreach ($subConjunto as $item) {

            $routeName = Route::currentRouteName();
            $class = "";
            if($routeName == $item->Ruta){
                $class = "star active open";
            }

            if ($todo->where('Parent', $item->idOpcion)->count() <= 0)
            {

                $menuHTML .= "<li class='nav-item $class'>";
                if ($item->EsLink == 1)
                {
                    if ($item->Ruta != "")
                    {
                        $menuHTML .= "<a href='" . route($item->Ruta) . "' class='nav-link nav-toggle'>";
                    }
                    else
                    {
                        $menuHTML .= "<a href='javascript:void(0)' class='nav-link nav-toggle'>";
                    }
                }
                else
                {
                    $menuHTML .= "<a href='javascript:void(0)' onclick='" . $item->Metodo . "' class='nav-link nav-toggle'>";
                }


                $menuHTML .= "<i class='" . $item->Imagen . "'></i>";
                $menuHTML .= "<span class='title'>" . $item->Descrip . "</span>";

                if($routeName == $item->Ruta){
                    $menuHTML .= "<span class='selected'></span>";
                }

                $menuHTML .= "</a>";
                $menuHTML .= "</li>";

            }

            if ($todo->where('Parent', $item->idOpcion)->count() > 0)
            {
                $menuHTML .= "<li class='nav-item $class'>";

                $menuHTML .= "<a href='#' class='nav-link nav-toggle'>";
                $menuHTML .= "<i class='" . $item->Imagen . "'></i>";
                $menuHTML .= "<span class='title'>" . $item->Descrip . "</span>";

                $menuHTML .= "<span class='arrow'></span>";
                $menuHTML .= "</a>";

                $menuHTML .= "<ul class='sub-menu'>";
                $menuHTML .= $this->armarMenuHTML($todo, $todo->where('Parent', $item->idOpcion), true);
                $menuHTML .= "</ul>";

                $menuHTML .= "</li>";
            }

        }
        if ($hijo == false)
        {

        }
        return $menuHTML;

    }

    protected function realizarPeticion($metodo, $url, $parametros=[])
    {

       $cliente = new Client(['curl' => [CURLOPT_CAINFO => base_path('resources/certs/cacert.pem')] ]);

        $respuesta = $cliente->request($metodo, $url, $parametros);

        return $respuesta->getBody()->getContents();
    }

    protected function obtenerAccessToken()
    {
        $clientId     = '1'/* config('api.client_id') */;
        $clientSecret = 'AkXfuQimD06Wv9eYWIJ7nERXwXc3en9v4G6zE3zn'/* config('api.client_secret') */;
    	$grantType    =  'client_credentials' /* config('api.grant_type') */;

        //dd("Aquiiiii " . $grantType,$clientSecret, $clientId );
		$respuesta = json_decode($this->realizarPeticion('POST', 'http://apiprofit_new.test/oauth/token',
		[
			'headers' => [
				'User-Agent' => 'testing/1.0',
        		'Accept'     => 'application/json',
				'X-Foo'      => ['Bar', 'Baz']

			],
			'form_params' => [
				'grant_type' => $grantType, 'client_id' => $clientId, 'client_secret' => $clientSecret
				]
		]));

		$accessToken = $respuesta->access_token;
    
    	return  $accessToken  ;
    }

    protected function getZonas()
    {
         $result = $this->realizarPeticion('GET', 'http://apiprofit_new.test/zonas', ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ],]);
        
         $get = json_decode($result);
         $zonas = $get->data;

         return $zonas;
    }

    protected function getSegmentos()
    {
         $result = $this->realizarPeticion('GET', 'http://apiprofit_new.test/segmentos', ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ],]);
         $get = json_decode($result);
         $segmentos = $get->data;

         return $segmentos;

    }

    protected function getTiposClientes()
    {
         $result = $this->realizarPeticion('GET', 'http://apiprofit_new.test/tiposclientes', ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ],]);
         $get = json_decode($result);
         $tiposclientes = $get->data;

         return $tiposclientes;

    }
    protected function getVendedores()
    {
         $result = $this->realizarPeticion('GET', 'http://apiprofit_new.test/vendedores', ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ],]);
         $get = json_decode($result);
         $vendedores = $get->data;
      
         return $vendedores;

    }
    protected function getCondicionPago()
    {
         $result = $this->realizarPeticion('GET', 'http://apiprofit_new.test/condicionesdepago', ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ],]);
         $get = json_decode($result);
         $condicionpago = $get->data;
      
         return $condicionpago;

    }
    protected function getListaPrecio()
    {
         $result = $this->realizarPeticion('GET', 'http://apiprofit_new.test/listaprecios', ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ],]);
         $get = json_decode($result);
         $listaprecio = $get->data;
      
         return $listaprecio;

    }
    protected function getProductos()
    {
         $result = $this->realizarPeticion('GET', 'http://apiprofit_new.test/productoslista', ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ],]);
         $get = json_decode($result);
         $productos = $get->data;
      
         return $productos; 

    }

    protected function getPedidosall()
    {
         $result = $this->realizarPeticion('GET', 'http://apiprofit_new.test/pedidos', ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ],]);
         $get = json_decode($result);
         $pedidos = $get->data;
      
         return $pedidos;

    }
    protected function getClientesView($id = 0)
    {
        $idvendedor = Auth::user()->id_vendedor;
    
        if (Input::has('idcliente')) {
            
            $respuesta = [];
            $respuesta = $this->realizarPeticion('GET', "http://apiprofit_new.test/clientes/{$id}", 
            ['headers' => [
                'User-Agent' => 'testing/1.0',
                'Accept'     => 'application/json',
                'X-Foo'      => ['Bar', 'Baz']
            ],]);
        $datos = json_decode($respuesta);
        $clientes = $datos->data;
        }else{
         $respuesta = $this->realizarPeticion('GET', "http://apiprofit_new.test/clientes",
         ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']
        ],]);

        $datos = json_decode($respuesta);
        $clientes = $datos->data;
        }
    
         return $clientes;

    }  
    
    protected function getClienteVendedor($idvendedor = null)
    {
        $idvendedor = Auth::user()->vendedor_id;
      if($idvendedor != 0){
            $respuesta = [];
            $respuesta = $this->realizarPeticion('GET', "http://apiprofit_new.test/vendedores/{$idvendedor}/clientes", 
            ['headers' => [
                'User-Agent' => 'testing/1.0',
                'Accept'     => 'application/json',
                'X-Foo'      => ['Bar', 'Baz']
            ],]);
        $datos = json_decode($respuesta);
        $clientevendedor = $datos->data;
      }else{
            $respuesta = [];
            $respuesta = $this->realizarPeticion('GET', "http://apiprofit_new.test/clientes", 
            ['headers' => [
                'User-Agent' => 'testing/1.0',
                'Accept'     => 'application/json',
                'X-Foo'      => ['Bar', 'Baz']
            ],]);
        $datos = json_decode($respuesta);
        $clientevendedor = $datos->data;
      }

         return $clientevendedor;

    }   
}
