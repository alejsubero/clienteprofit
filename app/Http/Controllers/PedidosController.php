<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Models\Pedidos;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PedidosController extends Controller
{
    public function __constructor()
    {

    }

    public function index()
    {
       
        $productos = $this->getProductos();
        //dd($productos);
        $clientevendedor = $this->getClienteVendedor();
        
        $clientes  = $this->getClientesView();

    	return view('pedidos.index',  compact('productos','clientes','clientevendedor'));

    }

    public function datosClientes(Request $request)
    {
        $id = $request->input('idcliente');
       
        $datosclientes  = $this->getClientesView($id);
       
        return response()->json($datosclientes); 

    }

    public function obtenerProductos(Request $request)
    {
       
        $productos  = $this->getProductos();
        $response = ['list' => $productos];
        return response()->json($response); 

    }

    public function savePedido(Request $request)
    {
  
        $producto = json_decode($request->input('producto'), true);
 
/*         foreach($producto as $product){
                $idproducto     = $product['idproducto'];
                $cliente_id     = $product['cliente_id'];
                $descripcion    = $product['descripcion'];
                $precio         = $product['precio'];
                $cantidad       = $product['cantidad'];
                $tot            = $product['tot'];
            
                $producto = new Productos();
                $producto->idproducto = $idproducto;
                $producto->cliente_id = $cliente_id;
                $producto->descripcion = $descripcion;
                $producto->precio = $precio;
                $producto->cantidad = $cantidad;
                $producto->tot = $tot;
                $producto->save();
            } */
            $accessToken = 'Bearer ' . $this->obtenerAccessToken();
            $respuesta = $this->realizarPeticion('POST', 'http://apiprofit_new.test/pedidodetalles',
             ['headers' =>['Authorization' => $accessToken],
              [
                'User-Agent' => 'testing/1.0',
                'Accept'     => 'application/json',
                'X-Foo'      => ['Bar', 'Baz']
    
            ], 'form_params' => $producto]);
    
            return response()->json($respuesta);
                //return response()->json($producto); 

    
    }

    public function getPedidos()
    {
        $pedido = Pedidos::all();

        $response = ['list' => $pedido];
        return response()->json($response);
    }

    public function searchProducto()
    {

         $texto = Input::get('texto');
    /*     //dd($texto); 
        $productosall = new Collection($this->getProductos());
       // dd($productosall);
        foreach($productosall as $product){
          // dd($product->descripcion);
            $p = ('select from '.$product->descripcion. 'like %'.$texto.'%');
          dd($p);
        } */
   
        return response()->json($texto);
        
 
      
    }

   
}
