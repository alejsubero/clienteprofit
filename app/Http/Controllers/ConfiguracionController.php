<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Configuracion;

class ConfiguracionController extends Controller
{
    public function __constructor()
    {
       
    }

    public function index()
    {
       $zonas               = $this->getZonas();
       $segmentos           = $this->getSegmentos();
       $tipocliente         = $this->getTiposClientes();
       $vendedores          = $this->getVendedores();
       $listaprecio         = $this->getListaPrecio();
       $condiciondepago     = $this->getCondicionPago();

        return view('configuracion.index', compact('zonas','segmentos','tipocliente','vendedores','listaprecio','condiciondepago'));

    }

    public function save()
    {
        //Se declaran todos los campos a guardar
        $configuser = Configuracion::firstOrNew(array('id' => Input::get('id')));
        $configuser->user_id            = Input::get("user_id");
        $configuser->id_vendedor        = Input::get("id_vendedor");
        $configuser->id_zona            = Input::get("id_zona");
        $configuser->id_tipo_cliente    = Input::get("id_tipo_cliente");
        $configuser->id_segmento        = Input::get("id_segmento");

        if($configuser->save()) {
            $response = ['data' => $configuser, 'success' => true, 'id' => $configuser->id, 'messaje' => 'Configuración creada correctamente.'];

        } else {
            $response = ['data' => [], 'success' => false, 'id' => -1, 'messaje' => 'Error al crear la Configuración.'];
        }

        return response()->json($response);

    }
}
