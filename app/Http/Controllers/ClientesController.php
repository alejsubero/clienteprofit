<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Models\Configuracion;

class ClientesController extends Controller
{
    public function __constructor()
    {
       
    }

    public function index()
    {
       $zonas             = $this->getZonas();
       $segmentos         = $this->getSegmentos();
       $tipocliente       = $this->getTiposClientes();
       $condiciondepago   = $this->getCondicionPago();
       $listaprecio       = $this->getListaPrecio();
       $vendedores        = $this->getVendedores();

        return view('clientes.index', compact('zonas','segmentos','tipocliente','vendedores','listaprecio','condiciondepago' ));

    }

    public function getClientes(Request $request)
    {
  
        if (Input::has('id_cliente')) {
            $id                 = Input::get('id_cliente');
            $respuesta = [];
            $respuesta = $this->realizarPeticion('GET', "http://apiprofit_new.test/clientes/{$id}", 
            ['headers' => [
                'User-Agent' => 'testing/1.0',
                'Accept'     => 'application/json',
                'X-Foo'      => ['Bar', 'Baz']
            ],]);
        $datos = json_decode($respuesta);
        $clientes = $datos->data;
        }else{
         $respuesta = $this->realizarPeticion('GET', "http://apiprofit_new.test/clientes",
         ['headers' => [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']
        ],]);

        $datos = json_decode($respuesta);
        $clientes = $datos->data;
        }

        $response = ['list' => $clientes];
        return response()->json($response);

    }

    public function saveClientes(Request $request)
    {
       
        $accessToken = 'Bearer ' . $this->obtenerAccessToken();
        $respuesta = $this->realizarPeticion('POST', 'http://apiprofit_new.test/clientes',
         ['headers' =>['Authorization' => $accessToken],
          [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ], 'form_params' => $request->all()]);
      
        return response()->json($respuesta);
    }

    public function updateClientes(Request $request)
    {
        $id = $request->input('id_cliente');
    
        $accessToken = 'Bearer ' . $this->obtenerAccessToken();
        $respuesta = $this->realizarPeticion('PUT', "http://apiprofit_new.test/clientes/{$id}",
         ['headers' =>['Authorization' => $accessToken],
          [
            'User-Agent' => 'testing/1.0',
            'Accept'     => 'application/json',
            'X-Foo'      => ['Bar', 'Baz']

        ], 'form_params' => $request->all()]);
   
        return response()->json($respuesta);
    }

    public function delete(Request $request)
    {
        $accessToken = 'Bearer ' . $this->obtenerAccessToken();
        $id = $request->input('id_cliente');

        $respuesta = $this->realizarPeticion('DELETE', "http://apiprofit_new.test/clientes/{$id}",
         ['headers' =>
         ['Authorization' => $accessToken

        ],]);

        return response()->json($respuesta);


    }

    public function getConfiguracion()
    {
        $zona           = $this->getZonas();
        $segmento       = $this->getSegmentos();
        $tipocliente    = $this->getTiposClientes();
        $vendedor       = $this->getVendedores();
        $configUser     = Configuracion::where('user_id', Auth::user()->id)->get();
       
        return response()->json([
            'zona'       => $zona,
            'segmento'   => $segmento,
            'tipocliente'=> $tipocliente,
            'vendedor'   => $vendedor,
            'configUser' => $configUser,
            ]);
 
    }


}
