<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use App\Models\Roles_con_opciones;
use App\Models\Opciones;
use Illuminate\Support\Facades\DB;


class RolesController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->middleware('auth');
        Config::set('app.module', 'Roles');
    }

    public function index(){
        
        return view('roles.index');
    }

    public function get(Request $request, $idRol = null){

        $roles = [];
        $response = [];

        if($idRol == null){
            $start = $request->input('start');
            $length = $request->input('length');
            $search = $request->input('search')['value'];
            $draw = (int)$request->input('draw');

            /*Para el ordenamiento*/
            $orderCol = $request->input('order.0.column');
            $orderDir = $request->input('order.0.dir');
            if($orderCol == null){
                $orderCol = 0;
            }
            if($orderDir == null){
                $orderDir = 'asc';
            }
            /**/

            $columns = ['id', 'name', 'slug', 'description'];
            
            if($search != null){

                $roles = Role::where('name', 'like', '%' . $search . '%')
                ->orWhere('slug', 'like', '%' . $search . '%')
                ->orWhere('description', 'like', '%' . $search . '%');
                
                $roles = $roles->orderBy($columns[$orderCol], $orderDir)->skip($start)->take($length)->get($columns);
            }else{

                $roles = Role::orderBy($columns[$orderCol], $orderDir)->skip($start)->take($length)->get($columns);

            }            

            $recordsTotal = Role::count();
            $recordsFiltered = $roles->count();

            $response = ['data' => $roles, 'recordsFiltered' => $recordsFiltered, 'recordsTotal' => $recordsTotal, 'draw' => $draw, '$search' => ''];
        }else{
            $roles = Role::where('id', $idRol)->get();
            $response = ['data' => $roles];
        }
       
        return response()->json($response);
    }

    public function getComboRoles(){
        
        $TipoDocumento = Role::all(['id AS val', 'name AS text']);
        $response = ['list' => $TipoDocumento];

        return response()->json($response);
    }

    public function save(){

        $rol = Role::firstOrNew(array('id' => Input::get('id')));

        $rol->name = Input::get("name");
        $rol->slug = Input::get("slug");
        $rol->description = Input::get("description");

        if($rol->save()) {
            $response = ['data' => $rol, 'success' => true, 'id' => $rol->id, 'messaje' => 'Rol creado correctamente.'];
        } else {
            $response = ['data' => [], 'success' => false, 'id' => -1, 'messaje' => 'Error al crear el Rol.'];
        }

        return response()->json($response);

    }

    public function delete($id = null){
        
        $rol = Role::find($id);

        if($rol->delete()) {
            $response = ['success' => true, 'id' => $rol->id, 'messaje' => 'Rol eliminado.'];
        } else {
            $response = ['success' => false, 'id' => -1, 'messaje' => 'Error al eliminar el rol.'];
        }

        return response()->json($response);
    }

    public function getPermisos($idRol = null){
        $sql = "select
                    p.id, ".$idRol." as idRol, p.name, p.slug, p.description,
                    IF(EXISTS(select id from permission_role as pr where pr.permission_id = p.id and pr.role_id = ".$idRol."), 'checked', '') as chk 
                from 
                    permissions as p;";

        $per = DB::select($sql);

        $response = ['data' => $per];

        return response()->json($response);
    }

    public function AddPermission(){

        $idRol = Input::get("idRol");
        $idPermission = Input::get("idPermission");
        
        $rol = Role::find($idRol);
        $rol->assignPermission($idPermission);
        $opciones = Opciones::where('permission_id', $idPermission)->first();
       
        $RCO = new roles_con_opciones;
        $RCO->role_id = $idRol;
        $RCO->con_opcion_id = $opciones->id;
        $RCO->save();
        
        if($rol->save()) {
            $response = ['success' => true, 'id' => $rol->id, 'messaje' => 'Permiso asignado.'];
        } else {
            $response = ['success' => false, 'id' => -1, 'messaje' => 'Error al asignar permiso.'];
        }

        return response()->json($response);

    }

    public function revokePermission(){
        
        $idRol = Input::get("idRol");
        $idPermission = Input::get("idPermission");
        
        $rol = Role::find($idRol);
        $rol->revokePermission($idPermission);
        
        if($rol->save()) {
            $response = ['success' => true, 'id' => $rol->id, 'messaje' => 'Permiso revocado.'];
        } else {
            $response = ['success' => false, 'id' => -1, 'messaje' => 'Error al revocar permiso.'];
        }

        return response()->json($response);

    }

}
