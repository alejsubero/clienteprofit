<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Login;
use Carbon\Carbon;

class ValidarUnicaSesion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()){
            $session_id   = \Session::getId();
            $last_sessionId = Auth::user()->ultima_sesion;
            if ($session_id != $last_sessionId) {

                $login = Login::find(session()->get('idLogin'));
                if($login){
                    $login->fecha_logout = Carbon::now();
                    $login->activa = 0;
                    $login->save();
                }

                Auth::logout();
                return redirect('/login');
            }
        }
        return $next($request);
    }
}