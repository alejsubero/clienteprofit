@extends('layouts.app') 
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Blog</span>
                    <span class="caption-helper">Consumiendo API</span>
                </div>
            </div>
<body>
	<div class="container">
		<h1>Publicaciones</h1>
		@foreach ($posts as $post)

		<div class="panel panel-default">

			<div class="panel-body">

				<a href="./post/{{ $post->id }}">

						{{ $post->title }}

				</a>

			
				
			</div>

		</div>

		@endforeach				
	</div>
	
</body>
        </div>

    </div>
</div>

@endsection




