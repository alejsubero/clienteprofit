@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Cambiar Contraseña</span>
                </div>

            </div>
            <div class="portlet-body">
       <form action="#" id="formCRUD" role="form">
               <div class="row">
               <div class="col-md-6">
                   <div class="form-group">
                       <label>Nueva Contraseña</label>
                       <input type="password" class="form-control" id="password" name="password">
                   </div>
               </div>
           </div>
            <div class="row">
               <div class="col-md-6">
                   <div class="form-group">
                       <label>Confirme Contraseña</label>
                       <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                   </div>
               </div>
           </div>



       </form>
            <div class="form-actions">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button class="btn green button-submit" id="btUpdatePassword"> Guardar
          <i class="fa fa-check"></i>
        </button>
      </div>

            </div>
        </div>

    </div>
</div>


@endsection
