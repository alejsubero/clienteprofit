<!--Modal para la modificar la contraseña de un usuario-->
<div id="mCRUDEdit" class="modal fade bs-modal-lg" tabindex="-1" data-backdrop="static" data-focus-on="input:first">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Rol</h4>
            </div>
            
            <div class="modal-body" >
                
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tabRol" aria-controls="tabRol" role="tab" data-toggle="tab">Rol</a></li>
                        <li role="presentation"><a href="#tabPermisos" aria-controls="tabPermisos" role="tab" data-toggle="tab">Asignar Permisos</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tabRol">
                            <form action="#" id="formCRUDEdit" role="form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-body">

                                    <div class="row hide">
                                        <div class="col-md-12">
                                            <input type="text" id="id" name="id" class="form-control">
                                        
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nombre:</label>
                                                <input type="text" id="name" name="name" class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Abreviación:</label>
                                                <input type="text" id="slug" name="slug" class="form-control" required>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Descripción:</label>
                                                <textarea id="description" name="description" class="form-control" rows="2"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </form>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="tabPermisos">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped table-hover" id="dtPermisos">
                                        <thead>
                                            
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                
            </div>
            <div class="modal-footer">
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="javascript:;" id="btCancelar2" class="btn green"> Cancelar
                                <i class="fa fa-close"></i>
                            </a>
                            <a href="javascript:;" id="btGuardar" class="btn green btGuardar" data-form="Edit"> Guardar
                                <i class="fa fa-check"></i>
                            </a>
                        </div>
                    </div>
                </div>
                    
            </div>
            

        </div>
    </div>
</div>
<style>
    
    [aria-labelledby="select2-IdIVA-container"], [aria-labelledby="select2-idTipoDoc-container"] { 
        background-color: #a2ffc4 !important;
    }
</style>