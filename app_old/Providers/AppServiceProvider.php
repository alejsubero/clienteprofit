<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use GuzzleHttp\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(179);

        app('view')->composer('layouts.app', function ($view) {
            $action = app('request')->route()->getAction();

            $routeName = app('request')->route()->getName();

            $controller = strtolower(str_replace('Controller', '', class_basename($action['controller'])));

            list($controller, $action) = explode('@', $controller);

            $view->with(compact('routeName', 'controller', 'action'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('GuzzleHttp\Client', function(){

            return new Client([

        'base_uri' => 'http://jsonplaceholder.typicode.com',
        //'base_uri' => 'http://apiprofit.filicabh.com.ve',
       // 'timeout'  => 2.0,
        ]);


        });
    }
   /* public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    // ...
    }*/
}
