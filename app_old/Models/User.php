<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Caffeinated\Shinobi\Traits\ShinobiTrait;

class User extends Authenticatable
{
    use Notifiable, ShinobiTrait;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombres', 'apellidos', 'email',  'facebook', 'telefono','color','Hab', 'password', 'role_id', 'role_name', 'created_at', 'updated_at', 'ultima_sesion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'Clave', 'remember_token', 'ClaveEnvio', 'ClaveFacebook',
        'password', 'remember_token',
    ];

    public function getAuthPassword (){
        return $this->password;
    }
}