<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Opciones;

class Roles_con_opciones extends Model
{
    protected $table = 'roles_con_opciones';
    
    protected $fillable = [
        'id', 'role_id', 'con_opcion_id', 'created_at', 'updated_at'
    ];

    protected $hidden = [
    ];

    public function opciones()
    {
        return $this->belongsToMany('App\Models\Opciones', 'opcion_id', 'id');
    }
}
