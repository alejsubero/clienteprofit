<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Auth;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Support\Facades\Route;
use App\Models\Opciones;
use App\Models\Roles_con_opciones;
use App\Models\Menu;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
//use GuzzleHttp\Psr7\Request;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {

        if (Auth::check()){
            $this->menu();
        }

    }

    public function menu(){

        $opciones = Menu::where('role_id', Auth::user()->role_id)->get();

        $m = $this->armarMenuHTML($opciones, $opciones->where('Parent', 0), false);
        session()->put('menu', $m);
    }

    public function armarMenuHTML($todo, $subConjunto, $hijo){

        $menuHTML = "";
        if ($hijo == false)
        { }
        else
        {  }

        foreach ($subConjunto as $item) {

            $routeName = Route::currentRouteName();
            $class = "";
            if($routeName == $item->Ruta){
                $class = "star active open";
            }

            if ($todo->where('Parent', $item->idOpcion)->count() <= 0)
            {

                $menuHTML .= "<li class='nav-item $class'>";
                if ($item->EsLink == 1)
                {
                    if ($item->Ruta != "")
                    {
                        $menuHTML .= "<a href='" . route($item->Ruta) . "' class='nav-link nav-toggle'>";
                    }
                    else
                    {
                        $menuHTML .= "<a href='javascript:void(0)' class='nav-link nav-toggle'>";
                    }
                }
                else
                {
                    $menuHTML .= "<a href='javascript:void(0)' onclick='" . $item->Metodo . "' class='nav-link nav-toggle'>";
                }


                $menuHTML .= "<i class='" . $item->Imagen . "'></i>";
                $menuHTML .= "<span class='title'>" . $item->Descrip . "</span>";

                if($routeName == $item->Ruta){
                    $menuHTML .= "<span class='selected'></span>";
                }

                $menuHTML .= "</a>";
                $menuHTML .= "</li>";

            }

            if ($todo->where('Parent', $item->idOpcion)->count() > 0)
            {
                $menuHTML .= "<li class='nav-item $class'>";

                $menuHTML .= "<a href='#' class='nav-link nav-toggle'>";
                $menuHTML .= "<i class='" . $item->Imagen . "'></i>";
                $menuHTML .= "<span class='title'>" . $item->Descrip . "</span>";

                $menuHTML .= "<span class='arrow'></span>";
                $menuHTML .= "</a>";

                $menuHTML .= "<ul class='sub-menu'>";
                $menuHTML .= $this->armarMenuHTML($todo, $todo->where('Parent', $item->idOpcion), true);
                $menuHTML .= "</ul>";

                $menuHTML .= "</li>";
            }

        }
        if ($hijo == false)
        {

        }
        return $menuHTML;

    }

    protected function realizarPeticion($metodo, $url, $parametros=[])
    {

       $cliente = new Client(['curl' => [CURLOPT_CAINFO => base_path('resources/certs/cacert.pem')]]);

        $respuesta = $cliente->request($metodo, $url, $parametros);

        return $respuesta->getBody()->getContents();
    }


    protected function obtenerAccessToken()
    {

        $clientId = config('api.client_id');

        $clientSecret = config('api.client_secret');

        $grantType = config('api.grant_type');

        //dd($clientId, $clientSecret, $grantType);
   // try{
        $respuesta = json_decode($this->realizarPeticion('GET', 'http://apiprofit.test:80/clientes',[]
       ));


    	$accessToken = $respuesta->data;

        return $accessToken;
    /* }catch (ClientException $e) {
        echo $e->getMessage() . "\n";
        echo $e->getRequest()->getMethod();
    }*/
    }

}
