<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

use App\Models\User as Usuario;
use App\Models\V_user;

class UsuariosController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth');
        Config::set('app.module', 'Usuarios');
    }

    public function index(){

        return view('usuarios.index');
    }

    public function get(Request $request, $idUsuario = null){

        $usuarios = [];
        $response = [];

        if($idUsuario == null){
            $start = $request->input('start');
            $length = $request->input('length');
            $search = $request->input('search')['value'];
            $draw = (int)$request->input('draw');

            /*Para el ordenamiento*/
            $orderCol = $request->input('order.0.column');
            $orderDir = $request->input('order.0.dir');
            if($orderCol == null){
                $orderCol = 0;
            }
            if($orderDir == null){
                $orderDir = 'asc';
            }
            /**/

            $hab = null;
            if(array_key_exists('hab', $request->input('search'))){
                $hab = $request->input('search')['hab'];
            }else{
                $hab = 1;
            }
            if($hab == -1){
                $hab = null;
            }

            $columns = ['id', 'nombres', 'apellidos', 'email', 'facebook', 'telefono', 'color'];

            if($search != null){

                $usuarios = V_user::all()
                ->where(function($q) use($search) {
                    $q->where('nombres', 'like', '%' . $search . '%')
                    ->orWhere('apellidos', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%')
                    ->orWhere('facebook', 'like', '%' . $search . '%');
                });

               /* if($hab != null){
                    $usuarios = $usuarios->where('Hab', '=', (int)$hab);
                }*/

                $usuarios = $usuarios->orderBy($columns[$orderCol], $orderDir)->skip($start)->take($length)->get($columns);

            }else{

                $usuarios = V_user::all();

                /*if($hab != null){
                    $usuarios = $usuarios->where('Hab', '=', $hab);
                }*/

            }

            $recordsTotal = V_user::all()->count();
            $recordsFiltered = $usuarios->count();

            $response = ['data' => $usuarios, 'recordsFiltered' => $recordsFiltered, 'recordsTotal' => $recordsTotal, 'draw' => $draw];//, '$hab' => $hab

        }else{

            $usuarios = V_user::where('id', $idUsuario)->get();
            $response = ['data' => $usuarios];
        }



        return response()->json($response);
    }

    public function save(){


        $usuario = Usuario::firstOrNew(array('id' => Input::get('id')));

        $usuario->nombres = Input::get('nombres');
        $usuario->apellidos = Input::get('apellidos');
        $usuario->password = Hash::make(Input::get('password'));
        $usuario->email = Input::get('email');
        $usuario->telefono = Input::get('telefono');
        $usuario->facebook = Input::get('facebook');
        $usuario->Hab = Input::get('Hab');
        $usuario->role_id = (int)Input::get('role_id');
        $usuario->color = Input::get('color');

        $user = null;
        if($usuario->save()) {

            $usuario->revokeAllRoles();
            $usuario->assignRole($usuario->role_id);

            $response = ['data' => $usuario, 'success' => true, 'id' => $usuario->id, 'messaje' => 'Usuario guardado.', '$user' => $usuario->id];
        } else {
            $response = ['data' => [], 'success' => false, 'id' => -1, 'messaje' => 'Error al guardar.'];
        }

        return response()->json($response);

    }

    public function delete($id = null)
    {

        $usuario = Usuario::find($id);

        if($usuario->delete()) {
            $response = ['data' => $usuario, 'success' => true, 'id' => $usuario->id, 'messaje' => 'Usuario eliminado.'];
        } else {
            $response = ['data' => [], 'success' => false, 'id' => -1, 'messaje' => 'Usuario no Eliminado.'];
        }

        return response()->json($response);
    }

        public function password()
    {
        return view('usuarios.password');
    }

    public function updatepassword(Request $request)
    {

     $user = Auth::user();
     $user->password = Hash::make($request->get('password'));


     if ($user->save()) {

         $response = ['data' => $user, 'success' => true, 'id' => $user->password, 'messaje' => 'contraseña Actualizada correctamente.'];
     }


        return response()->json($response);

   // }

    }

}
