-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-10-2018 a las 07:30:06
-- Versión del servidor: 10.1.24-MariaDB
-- Versión de PHP: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sem_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre_cliente` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cirif` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_cliente` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_cliente2` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_cliente` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `web_cliente` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion_cliente` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `con_login`
--

CREATE TABLE `con_login` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `id_session` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activa` tinyint(4) NOT NULL DEFAULT '1',
  `fecha_login` datetime NOT NULL,
  `fecha_logout` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `con_login`
--

INSERT INTO `con_login` (`id`, `user_id`, `id_session`, `activa`, `fecha_login`, `fecha_logout`, `created_at`, `updated_at`) VALUES
(25, 3, '4xIUcJHfzg6f3pdWDvl8YdEqt8v0Mh6WaUkWuRHk', 1, '2018-09-14 05:40:20', '0000-00-00 00:00:00', '2018-09-14 09:40:20', '2018-09-14 09:40:20'),
(26, 3, 'CuwedYpz5cSui1dfYhFf2tnQLv1miZw7S3555EAj', 1, '2018-09-14 10:35:12', '0000-00-00 00:00:00', '2018-09-14 14:35:12', '2018-09-14 14:35:12'),
(27, 3, 'a2P6MAX1wub4fMdP8HXlOM9h3YGCOlMfzJ7DZIbr', 0, '2018-10-03 23:02:09', '2018-10-03 23:12:54', '2018-10-04 03:02:09', '2018-10-04 03:12:54'),
(28, 3, 'kOkvmLRjU6x2jSuvhGHs1E6znsqFf69iWz30MgOO', 1, '2018-10-03 23:52:30', '0000-00-00 00:00:00', '2018-10-04 03:52:30', '2018-10-04 03:52:30'),
(29, 3, 'N7PiT10tNclgZPV29T6wf5wnKrXwOgnCfZCBZLUv', 0, '2018-10-04 00:11:19', '2018-10-04 04:22:21', '2018-10-04 04:11:19', '2018-10-04 08:22:21'),
(30, 4, 'kZywSlRf8aQ5lUGAojWEgN7QCI88xt9M28XaEME4', 1, '2018-10-04 04:22:38', '0000-00-00 00:00:00', '2018-10-04 08:22:38', '2018-10-04 08:22:38'),
(31, 4, 'NVZrLMHbZBZkAlKzw8yG0R0J7dDVDVGkANtoJWNA', 1, '2018-10-07 02:59:07', '0000-00-00 00:00:00', '2018-10-07 06:59:07', '2018-10-07 06:59:07'),
(32, 3, 'GjAC3AxvamBuwwpH1qHBnMLAb8Cp8Ox98jOWLO61', 1, '2018-10-12 15:33:46', '0000-00-00 00:00:00', '2018-10-12 19:33:46', '2018-10-12 19:33:46'),
(33, 3, 'xWtoBm4iuEmqZj4GELE7bRmB3IusRGLgtjex4Hs7', 0, '2018-10-13 13:48:32', '2018-10-13 13:59:46', '2018-10-13 17:48:32', '2018-10-13 17:59:46'),
(34, 3, 'deS2WHQ9DY46xCvnVHUYqwSPZPQXO3zej5XfHtgQ', 0, '2018-10-13 14:00:34', '2018-10-13 14:07:41', '2018-10-13 18:00:34', '2018-10-13 18:07:41'),
(35, 3, 'WaMJEsUmWtjdgITM4jQ3oWek0KhUX32u7tpZTr24', 0, '2018-10-13 14:27:49', '2018-10-13 14:27:59', '2018-10-13 18:27:49', '2018-10-13 18:27:59'),
(36, 3, 'MJ8NqTx36xsQcaQ2ILPd8rQNqEgYZ1vQTJr8uobj', 0, '2018-10-13 14:33:53', '2018-10-13 14:34:01', '2018-10-13 18:33:53', '2018-10-13 18:34:01'),
(37, 3, 'vDndl1YvYm5zIABArjchjmNa8pZpmmn0T6GlIdoJ', 0, '2018-10-13 14:43:02', '2018-10-13 14:43:07', '2018-10-13 18:43:02', '2018-10-13 18:43:07'),
(38, 3, 'ltRdsEGHPimvHstkIyLtCeoI8zNeeOBbymJTaj2U', 0, '2018-10-13 15:08:29', '2018-10-13 15:08:37', '2018-10-13 19:08:29', '2018-10-13 19:08:37'),
(39, 3, 'JPt5KkIdWlB8PVKpAXg0FyN2uoOnsRaWh6dQwKXW', 0, '2018-10-13 15:58:32', '2018-10-13 15:58:55', '2018-10-13 19:58:32', '2018-10-13 19:58:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `con_opciones`
--

CREATE TABLE `con_opciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `idOpcion` int(11) NOT NULL,
  `Descrip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Parent` int(11) NOT NULL,
  `Nivel` int(11) NOT NULL,
  `Orden` int(11) NOT NULL,
  `Imagen` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Hab` tinyint(4) NOT NULL,
  `Ruta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EsLink` tinyint(4) NOT NULL,
  `Metodo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `con_opciones`
--

INSERT INTO `con_opciones` (`id`, `idOpcion`, `Descrip`, `Parent`, `Nivel`, `Orden`, `Imagen`, `Hab`, `Ruta`, `EsLink`, `Metodo`, `permission_id`) VALUES
(1, 100, 'Inicio', 0, 0, 500, 'icon-home', 1, 'home', 1, '1', 1),
(2, 200, 'Administrar Usuarios', 0, 1, 1000, 'fa fa-users', 1, '', 1, '1', 2),
(3, 210, 'Roles de Usuarios', 200, 2, 1020, 'icon-user', 1, 'roles::index', 1, '1', 3),
(4, 220, 'Usuario Nuevo', 200, 2, 1040, 'fa fa-user', 1, 'usuarios::index', 1, '1', 4),
(5, 300, 'Productos', 0, 0, 1500, 'fa fa-book', 1, 'productos::index', 1, '1', 5),
(6, 400, 'Ventas', 0, 0, 2000, 'icon-docs', 1, '', 1, '1', 6),
(7, 500, 'Clientes', 0, 0, 2500, 'fa fa-user', 1, '', 1, '1', 7),
(8, 600, 'Reportes de Ventas', 0, 0, 3000, 'icon-bar-chart', 1, '', 1, '1', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(179) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(153, '2012_01_20_084450_create_roles_table', 1),
(154, '2012_01_24_080208_create_permissions_table', 1),
(155, '2012_02_24_080433_create_permission_role_table', 1),
(156, '2013_07_12_000000_create_estados_table', 1),
(157, '2013_08_12_000000_create_municipios_table', 1),
(158, '2013_08_14_102053_create_opciones', 1),
(159, '2014_10_12_000000_create_users_table', 1),
(160, '2014_10_12_100000_create_password_resets_table', 1),
(161, '2015_01_20_084525_create_role_user_table', 1),
(162, '2015_12_04_003040_add_special_role_column', 1),
(163, '2017_10_17_170735_create_permission_user_table', 1),
(164, '2018_08_13_202053_create_registro_login_conectados', 1),
(165, '2018_08_14_202053_create_roles_con_opciones', 1),
(166, '2018_08_23_102053_create_vista_roles_opciones', 1),
(167, '2018_08_23_302053_create_clientes', 1),
(168, '2018_08_23_402053_create_estructuras', 1),
(169, '2018_08_24_150727_create_ofertas_table', 1),
(170, '2018_08_24_224220_create_orden_trabajo_despachos_table', 1),
(171, '2018_08_26_005353_create_orden_despacho_general_table', 1),
(172, '2018_08_26_010205_create_piezas_table', 1),
(173, '2018_08_27_151056_create_vista_user_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(179) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(179) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(179) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(179) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Inicio', 'Ini', 'Home de la aplicación', '2018-10-04 06:12:24', '2018-10-04 06:12:24'),
(2, 'Administrar Usuarios', 'Admin. Usu', 'Sección para administrar todo lo relacionado a los usuarios del sistema', '2018-10-04 06:43:48', '2018-10-04 06:43:48'),
(3, 'Roles de Usuarios', 'roles', 'Permite administrar los roles de los usuarios', '2018-10-04 06:44:52', '2018-10-04 06:44:52'),
(4, 'Usuario Nuevo', 'Usu. New', 'Crear, editar y eliminar usuarios del sistema', '2018-10-04 06:46:41', '2018-10-04 06:46:41'),
(5, 'Productos', 'VE', 'Este rol será asignado a los vendedores de la empresa', '2018-10-04 07:08:26', '2018-10-04 07:11:48'),
(6, 'Ventas', 'ventas', 'Se registraran todas las ventas', '2018-10-04 07:24:56', '2018-10-04 07:24:56'),
(7, 'Clientes', 'clientes', 'Se registraran todos los clientes', '2018-10-04 07:25:30', '2018-10-04 07:25:30'),
(8, 'Reportes de Ventas', 'reportes', 'se visualizaran todos los reportes de las ventas', '2018-10-04 07:26:32', '2018-10-04 07:26:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-10-04 06:37:52', '2018-10-04 06:37:52'),
(2, 2, 1, '2018-10-04 06:51:12', '2018-10-04 06:51:12'),
(3, 3, 1, '2018-10-04 07:10:46', '2018-10-04 07:10:46'),
(4, 4, 1, '2018-10-04 07:10:47', '2018-10-04 07:10:47'),
(5, 5, 1, '2018-10-04 07:10:49', '2018-10-04 07:10:49'),
(6, 6, 1, '2018-10-04 07:32:00', '2018-10-04 07:32:00'),
(7, 7, 1, '2018-10-04 07:32:01', '2018-10-04 07:32:01'),
(8, 8, 1, '2018-10-04 07:32:02', '2018-10-04 07:32:02'),
(9, 1, 3, '2018-10-04 08:22:09', '2018-10-04 08:22:09'),
(10, 5, 3, '2018-10-04 08:22:13', '2018-10-04 08:22:13'),
(11, 6, 3, '2018-10-04 08:22:15', '2018-10-04 08:22:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(179) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(179) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `special` enum('all-access','no-access') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`, `special`) VALUES
(1, 'Administrador', 'Admin', 'Admin del Sistema', NULL, '2018-09-03 07:29:02', NULL),
(3, 'Vendedor', 'VE', 'Este rol será asignado a los vendedores de la empresa', '2018-10-04 08:14:08', '2018-10-04 08:14:08', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_con_opciones`
--

CREATE TABLE `roles_con_opciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `con_opcion_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles_con_opciones`
--

INSERT INTO `roles_con_opciones` (`id`, `role_id`, `con_opcion_id`, `created_at`, `updated_at`) VALUES
(5, 1, 1, '2018-10-04 06:37:52', '2018-10-04 06:37:52'),
(6, 1, 2, '2018-10-04 06:51:12', '2018-10-04 06:51:12'),
(7, 1, 3, '2018-10-04 07:10:46', '2018-10-04 07:10:46'),
(8, 1, 4, '2018-10-04 07:10:47', '2018-10-04 07:10:47'),
(9, 1, 5, '2018-10-04 07:10:49', '2018-10-04 07:10:49'),
(10, 1, 6, '2018-10-04 07:32:01', '2018-10-04 07:32:01'),
(11, 1, 7, '2018-10-04 07:32:01', '2018-10-04 07:32:01'),
(12, 1, 8, '2018-10-04 07:32:03', '2018-10-04 07:32:03'),
(13, 3, 1, '2018-10-04 08:22:09', '2018-10-04 08:22:09'),
(14, 3, 5, '2018-10-04 08:22:13', '2018-10-04 08:22:13'),
(15, 3, 6, '2018-10-04 08:22:15', '2018-10-04 08:22:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, NULL, NULL),
(2, 3, 4, '2018-10-04 08:21:30', '2018-10-04 08:21:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombres` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidos` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(179) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Hab` tinyint(4) NOT NULL DEFAULT '1',
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ultima_sesion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombres`, `apellidos`, `email`, `password`, `facebook`, `telefono`, `color`, `Hab`, `role_id`, `remember_token`, `ultima_sesion`, `created_at`, `updated_at`) VALUES
(3, 'Alejandro', 'Subero', 'eulemisalfredo@gmail.com', '$2y$10$Vv1VJ1nojQsSQIiJp/YauOBofU7yrLvePV/7Hv4ohWih2avB8Ebza', 'eulemis_hdd_hernandez', '04242584644', '#ff7a00', 1, 1, 'PAOX8wObOl9yn0Ex79V2yzx7RS955BGfTZuTCCNyRzfL7sOeOTEDSpRH4yHO', 'JPt5KkIdWlB8PVKpAXg0FyN2uoOnsRaWh6dQwKXW', '2018-08-27 21:17:06', '2018-10-13 19:58:32'),
(4, 'Juan Diego', 'Subero', 'juandiego1410@gmail.com', '$2y$10$4g2TxiemcrFJ0moQ85KVLO6D1RNhLx8xJV096JDX7d/.78vAomn/C', NULL, '04123620196', '#0029ff', 1, 3, '', 'NVZrLMHbZBZkAlKzw8yG0R0J7dDVDVGkANtoJWNA', '2018-10-04 08:21:30', '2018-10-07 06:59:06');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vistauser`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vistauser` (
`id` int(10) unsigned
,`nombres` varchar(100)
,`apellidos` varchar(100)
,`email` varchar(60)
,`password` varchar(179)
,`facebook` varchar(100)
,`telefono` varchar(24)
,`color` varchar(12)
,`Hab` tinyint(4)
,`role_id` int(10) unsigned
,`remember_token` varchar(100)
,`ultima_sesion` varchar(255)
,`created_at` timestamp
,`updated_at` timestamp
,`role_name` varchar(179)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_roles_con_opciones`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_roles_con_opciones` (
`id` int(10) unsigned
,`idOpcion` int(11)
,`Descrip` varchar(100)
,`Parent` int(11)
,`Nivel` int(11)
,`Orden` int(11)
,`Imagen` varchar(200)
,`Hab` tinyint(4)
,`Ruta` varchar(255)
,`EsLink` tinyint(4)
,`Metodo` varchar(255)
,`role_id` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Estructura para la vista `vistauser`
--
DROP TABLE IF EXISTS `vistauser`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vistauser`  AS  select `cu`.`id` AS `id`,`cu`.`nombres` AS `nombres`,`cu`.`apellidos` AS `apellidos`,`cu`.`email` AS `email`,`cu`.`password` AS `password`,`cu`.`facebook` AS `facebook`,`cu`.`telefono` AS `telefono`,`cu`.`color` AS `color`,`cu`.`Hab` AS `Hab`,`cu`.`role_id` AS `role_id`,`cu`.`remember_token` AS `remember_token`,`cu`.`ultima_sesion` AS `ultima_sesion`,`cu`.`created_at` AS `created_at`,`cu`.`updated_at` AS `updated_at`,`r`.`name` AS `role_name` from ((`users` `cu` join `roles` `r`) join `role_user` `ru` on(((`cu`.`id` = `ru`.`user_id`) and (`r`.`id` = `ru`.`role_id`)))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_roles_con_opciones`
--
DROP TABLE IF EXISTS `v_roles_con_opciones`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_roles_con_opciones`  AS  select `o`.`id` AS `id`,`o`.`idOpcion` AS `idOpcion`,`o`.`Descrip` AS `Descrip`,`o`.`Parent` AS `Parent`,`o`.`Nivel` AS `Nivel`,`o`.`Orden` AS `Orden`,`o`.`Imagen` AS `Imagen`,`o`.`Hab` AS `Hab`,`o`.`Ruta` AS `Ruta`,`o`.`EsLink` AS `EsLink`,`o`.`Metodo` AS `Metodo`,`ro`.`role_id` AS `role_id` from (`con_opciones` `o` join `roles_con_opciones` `ro` on((`o`.`id` = `ro`.`con_opcion_id`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `con_login`
--
ALTER TABLE `con_login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `con_login_user_id_index` (`user_id`);

--
-- Indices de la tabla `con_opciones`
--
ALTER TABLE `con_opciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `con_opciones_permission_id_index` (`permission_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indices de la tabla `roles_con_opciones`
--
ALTER TABLE `roles_con_opciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_con_opciones_role_id_index` (`role_id`),
  ADD KEY `roles_con_opciones_con_opcion_id_index` (`con_opcion_id`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `con_login`
--
ALTER TABLE `con_login`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `con_opciones`
--
ALTER TABLE `con_opciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `roles_con_opciones`
--
ALTER TABLE `roles_con_opciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `con_login`
--
ALTER TABLE `con_login`
  ADD CONSTRAINT `con_login_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `con_opciones`
--
ALTER TABLE `con_opciones`
  ADD CONSTRAINT `con_opciones_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `roles_con_opciones`
--
ALTER TABLE `roles_con_opciones`
  ADD CONSTRAINT `roles_con_opciones_con_opcion_id_foreign` FOREIGN KEY (`con_opcion_id`) REFERENCES `con_opciones` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `roles_con_opciones_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
