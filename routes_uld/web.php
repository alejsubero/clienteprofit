<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

//PIEZAS////////////////////////////////

Route::group(['as'=>'productos::'], function(){
     Route::get('/productos', ['as' => 'index', 'uses' => 'ProductosController@index']);
   // Route::get('/producto',  'ProductosController@index')->name('productos');
      // Route::get('/estado', 'EstadosController@index')->name('estado');
    Route::get('/productos/get/{id?}', ['as' => 'get', 'uses' => 'ProductosController@get']);
    Route::post('/productos/duplicated', ['as' => 'save', 'uses' => 'ProductosController@duplicated']);
    Route::post('/productos/save', ['as' => 'save', 'uses' => 'ProductosController@save']);
    Route::get('/productos/delete/{id?}', ['as' => 'delete', 'uses' => 'ProductosController@delete']);
});


////////////////////////////////////////

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['as'=>'usuarios::'], function(){
    Route::get('/usuarios', ['as' => 'index', 'uses' => 'UsuariosController@index']);
    Route::get('/usuarios/get/{idUsuario?}', ['as' => 'get', 'uses' => 'UsuariosController@get']);
    Route::post('/usuarios/save', ['as' => 'save', 'uses' => 'UsuariosController@save']);
    Route::get('/usuarios/delete/{id?}', ['as' => 'delete', 'uses' => 'UsuariosController@delete']);
    Route::get('/usuarios/getComboSede', ['as' => 'getComboSede', 'uses' => 'UsuariosController@getComboSede']);
    Route::get('/usuarios/getComboPrograma', ['as' => 'getComboPrograma', 'uses' => 'UsuariosController@getComboPrograma']);
    Route::get('/usuarios/password', ['as' => 'password', 'uses' => 'UsuariosController@password']);
    Route::post('/usuarios/updatepassword', ['as' => 'updatepassword', 'uses' => 'UsuariosController@updatepassword']);

});

Route::group(['as'=>'roles::'], function(){
    Route::get('/roles', ['as' => 'index', 'uses' => 'RolesController@index']);
    Route::get('/roles/getComboRoles', ['as' => 'getComboRoles', 'uses' => 'RolesController@getComboRoles']);
    Route::get('/roles/get/{idRol?}', ['as' => 'get', 'uses' => 'RolesController@get']);
    Route::post('/roles/save', ['as' => 'save', 'uses' => 'RolesController@save']);
    Route::get('/roles/delete/{id?}', ['as' => 'delete', 'uses' => 'RolesController@delete']);
    Route::get('/roles/getPermisos/{idRol?}', ['as' => 'getPermisos', 'uses' => 'RolesController@getPermisos']);
    Route::post('/roles/AddPermission', ['as' => 'AddPermission', 'uses' => 'RolesController@AddPermission']);
    Route::post('/roles/revokePermission', ['as' => 'revokePermission', 'uses' => 'RolesController@revokePermission']);
});

Route::group(['as'=>'permisos::'], function(){
    Route::get('/permisos', ['as' => 'index', 'uses' => 'PermisosController@index']);
    Route::get('/permisos/getForCombo', ['as' => 'getForCombo', 'uses' => 'PermisosController@getForCombo']);
    Route::get('/permisos/get/{idRol?}', ['as' => 'get', 'uses' => 'PermisosController@get']);
    Route::post('/permisos/save', ['as' => 'save', 'uses' => 'PermisosController@save']);
    Route::get('/permisos/delete/{id?}', ['as' => 'delete', 'uses' => 'PermisosController@delete']);
});

Route::group(['as'=>'post::'], function(){
     Route::get('/post', ['as' => 'index', 'uses' => 'PostsController@index']);
     Route::get('/post/{id?}', [ 'as' => 'show', 'uses' => 'PostsController@show']);
});


*/
Route::get('/prueba', 'Controller@obtenerAccessToken');
 // Route::post('/prueba', ['as' => 'prueba', 'uses' => 'Controller@obtenerAccessToken']);

