<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenDespachoGeneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_despacho_general', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('num_orden');
            //$table->integer('num_oferta');
            $table->integer('cliente_id')->unsigned()->index();
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade');
            $table->string('cliente', 255);
            $table->integer('piezas_total');
            $table->integer('cantidad_pendiente');
            $table->double('peso_pendiente');
            $table->integer('cantidad_despachada');
            $table->double('peso_despachado');
            $table->double('total_peso');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_despacho_general');
    }
}
