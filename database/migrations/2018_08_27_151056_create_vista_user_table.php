<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVistaUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW VistaUser as select `cu`.`id` AS `id`, `cu`.`telefono` AS `telefono`,`cu`.`facebook` AS `facebook`, `cu`.`color` AS `color`,  `cu`.`Hab` AS `Hab`, `cu`.`nombres` AS `nombres`, `cu`.`apellidos` AS `apellidos`, `cu`.`role_id` AS `role_id`,`cu`.`email` AS `email`, `r`.`name` AS `role_name`,  `cu`.`password` AS `password` from `users` as `cu`  inner join `roles` `r` inner join role_user as ru on(`cu`.`id` = `ru`.`user_id` and `r`.`id` = `ru`.`role_id`)  group by `cu`.`id` ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW VistaUser");
    }
}
