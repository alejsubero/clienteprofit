<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateVistaRolesOpciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW v_roles_con_opciones as select `o`.`id` AS `id`,`o`.`idOpcion` AS `idOpcion`,`o`.`Descrip` AS `Descrip`,`o`.`Parent` AS `Parent`,`o`.`Nivel` AS `Nivel`,`o`.`Orden` AS `Orden`,`o`.`Imagen` AS `Imagen`,`o`.`Hab` AS `Hab`,`o`.`Ruta` AS `Ruta`,`o`.`EsLink` AS `EsLink`,`o`.`Metodo` AS `Metodo`,`ro`.`role_id` AS `role_id` from (`con_opciones` `o` join `roles_con_opciones` `ro` on((`o`.`id` = `ro`.`con_opcion_id`)))  ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW v_roles_con_opciones");
    }
}
