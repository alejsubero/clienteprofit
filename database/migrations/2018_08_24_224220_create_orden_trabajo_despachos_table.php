<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenTrabajoDespachosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_trabajo_despachos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero_orden');
            $table->integer('numero_correlativo');
            $table->string('numero_plano', 255);
            $table->string('tipo', 255);
            $table->integer('articulo');
            $table->string('elemento', 255);
            $table->integer('cantidad_asignada');
            $table->double('peso_elemento');
            $table->double('ancho');
            $table->double('longitud');
            $table->double('espesor');
            $table->double('total_peso');
            $table->text('colada');
            $table->string('calidad', 255);
            $table->string('marca', 255);
            $table->string('posicion', 255);
            $table->string('pieza', 255);
            $table->integer('cantidad_fabricada');
            $table->integer('cantidad_no_fabricada');//OJO aclarar
            $table->integer('cantidad_pintada');
            $table->integer('cantidad_no_pintada');//OJO aclarar
            $table->integer('cantidad_galvanizada');
            $table->integer('cantidad_no_galvanizada');//OJO aclarar
            $table->integer('cantidad_despachada');
            $table->integer('cantidad_no_despachada');//OJO aclarar
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_trabajo_despachos');
    }
}
