<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('users', function (Blueprint $table) {
            //$table->engine = 'MyISAM'; 
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('nombres', 100)->nullable();
            $table->string('apellidos', 100)->nullable();
            $table->string('email', 60)->unique();
            $table->string('password');
            $table->string('facebook', 100)->nullable();
            $table->string('telefono', 24);
            $table->string('color', 12);
            $table->tinyInteger('Hab')->default('1');
            $table->integer('role_id')->unsigned()->index();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->string('remember_token', 100);
            $table->string('ultima_sesion', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations. 
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
