<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('con_opciones', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('idOpcion');
            $table->string('Descrip', 100);
            $table->integer('Parent');
            $table->integer('Nivel');
            $table->integer('Orden');
            $table->string('Imagen', 200);
            $table->tinyInteger('Hab');
            $table->string('Ruta', 255);
            $table->tinyInteger('EsLink');
            $table->string('Metodo', 255);
            $table->integer('permission_id')->unsigned()->index();
            $table->foreign('permission_id')->references('id')->on('con_opciones')->onDelete('cascade');
            //ORIGINAL $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
            //$table->integer('permissions_id')->index();
            //$table->foreign('permissions_id')->references('id')->on('permissions')->onDelete('cascade');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('con_opciones');
    }
}
