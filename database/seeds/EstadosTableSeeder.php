<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Estados;

class EstadosTableSeeder extends Seeder {
	public function run()
	{
		Model::unguard();
		

	
		/* ESTADOS */
		Estados::create(array( 'id' => 1, 'estado' => 'Amazonas', 'iso_3166-2'=> 'VE-X' ));
		Estados::create(array( 'id' => 2, 'estado' => 'Anzoátegui', 'iso_3166-2'=> 'VE-B' ));
		Estados::create(array( 'id' => 3, 'estado' => 'Apure', 'iso_3166-2'=> 'VE-C' ));
		Estados::create(array( 'id' => 4, 'estado' => 'Aragua', 'iso_3166-2'=> 'VE-D' ));
		Estados::create(array( 'id' => 5, 'estado' => 'Barinas', 'iso_3166-2'=> 'VE-E' ));
		Estados::create(array( 'id' => 6, 'estado' => 'Bolívar', 'iso_3166-2'=> 'VE-F' ));
		Estados::create(array( 'id' => 7, 'estado' => 'Carabobo', 'iso_3166-2'=> 'VE-G' ));
		Estados::create(array( 'id' => 8, 'estado' => 'Cojedes', 'iso_3166-2'=> 'VE-H' ));
		Estados::create(array( 'id' => 9, 'estado' => 'Delta Amacuro', 'iso_3166-2'=> 'VE-Y' ));
		Estados::create(array( 'id' => 10, 'estado' => 'Falcón', 'iso_3166-2'=> 'VE-I' ));
		Estados::create(array( 'id' => 11, 'estado' => 'Guárico', 'iso_3166-2'=> 'VE-J' ));
		Estados::create(array( 'id' => 12, 'estado' => 'Lara', 'iso_3166-2'=> 'VE-K' ));
		Estados::create(array( 'id' => 13, 'estado' => 'Mérida', 'iso_3166-2'=> 'VE-L' ));
		Estados::create(array( 'id' => 14, 'estado' => 'Miranda', 'iso_3166-2'=> 'VE-M' ));
		Estados::create(array( 'id' => 15, 'estado' => 'Monagas', 'iso_3166-2'=> 'VE-N' ));
		Estados::create(array( 'id' => 16, 'estado' => 'Nueva Esparta', 'iso_3166-2'=> 'VE-O' ));
		Estados::create(array( 'id' => 17, 'estado' => 'Portuguesa', 'iso_3166-2'=> 'VE-P' ));
		Estados::create(array( 'id' => 18, 'estado' => 'Sucre', 'iso_3166-2'=> 'VE-R' ));
		Estados::create(array( 'id' => 19, 'estado' => 'Táchira', 'iso_3166-2'=> 'VE-S' ));
		Estados::create(array( 'id' => 20, 'estado' => 'Trujillo', 'iso_3166-2'=> 'VE-T' ));
		Estados::create(array( 'id' => 21, 'estado' => 'Vargas', 'iso_3166-2'=> 'VE-W' ));
		Estados::create(array( 'id' => 22, 'estado' => 'Yaracuy', 'iso_3166-2'=> 'VE-U' ));
		Estados::create(array( 'id' => 23, 'estado' => 'Zulia', 'iso_3166-2'=> 'VE-V' ));
		Estados::create(array( 'id' => 24, 'estado' => 'Distrito Capital', 'iso_3166-2'=> 'VE-A' ));
		Estados::create(array( 'id' => 25, 'estado' => 'Dependencias Federales', 'iso_3166-2'=> 'VE-Z' ));
		
	}
}