<?php

use Illuminate\Database\Seeder;
use App\Models\Estados;
use App\Models\Municipios;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

         //$this->call(UsersTableSeeder::class);
         $this->call(EstadosTableSeeder::class);
         $this->call(MunicipiosTableSeeder::class);
    }
}
