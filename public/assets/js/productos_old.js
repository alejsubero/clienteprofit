var productos = function () {
    var producto = {
        dt:[]  
    };

    var formCRUD = $('#formCRUD');

    var setJQueryValidate = function(){
        //jQuery.validator.setDefaults({
        formCRUD.validate({
            doNotHideMessage: true,
            errorElement: 'span',
            errorClass: 'help-block help-block-error',
            focusInvalid: true,
            ignoreTitle: true,
            //ignore: '',
            messages : {
                required: "Este campo es requerido.",
                number : "Por favor ingrese solo números."
            },
            rules : {

            },
            invalidHandler : function (event, validator) {
                //$('.alert-error', $('.login-form')).show();
            },
            errorPlacement: function (error, element) {
                var cont = $(element).parent('.input-group');

                /* if (cont) {
                    console.log(cont);
                    cont.after(error);
                } else {
                    console.log(element);
                    //element.after(error);
                    error.appendTo(element.closest(".form-group"));
                }*/
                error.appendTo(element.closest(".form-group"));
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (element) {
                element.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {

            }
        });

        //formCRUD.validate();
    }

       // $('#dtTableP').DataTable();

     var initDataTable = function(){

        var table = $('#dtTable');

        var oTable = table.dataTable({
            destroy:true,
            "language": languageDataTablet,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  $url + "/getPedidos",// ajax source
                "dataSrc": "list"
            },

            "columns": [

                { "data": "descripcion", "title": "Nombre Producto", "visible": true},
                { "data": "cantidad", "title": "Cantidad", "visible":true},
                { "data": "tot", "title": "Total", "visible": true},
            
        {
            "targets": -1,
            "data": null,
            "className" : "center",
            "width": "110px",
            "title":"Acción",
            "defaultContent": '<div class="text-center bt_dt">' +
            '<button type="button" class="btn btn-sm default blue modal-toggle" id="btEditar" title="Modificar">' +
            '<i class="fa fa-edit"></i>' +
            '</button>' +
            '<button type="button" class="btn btn-sm default red modal-toggle" id="btEliminar" title="Baja">' +
            '<i class="fa fa-close"></i>' +
            '</button>' +
            '</div>',
            "orderable": false
        }
        ],
        columnDefs: [ {
        }],
        select: "single",
        buttons: [
        { extend: 'copy', className: 'btn red btn-outline' },
        { extend: 'excel', className: 'btn yellow btn-outline ' },
        { extend: 'csv', className: 'btn purple btn-outline ' },
        { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'},
        {
            text: 'Recargar',
            className: 'btn default',
            action: function ( e, dt, node, config ) {
                dt.clearPipeline().draw();
            }
        },
        ],
        responsive: false,
        "order": [
        ],
        "lengthMenu": [
        [5, 10, 15, 20, -1],
                [5, 10, 15, 20, 60] // change per page values here
                ],
                "pageLength": 5,
            });


        $('#table_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });

        $('#dtTable tbody').on('click', '#btEditar, #btEliminar', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();
            if(this.id == "btEditar"){

                editar(data);

            }
            if(this.id == "btEliminar"){
                bootbox.dialog({
                    message: "¿Está seguro de eliminar el registro seleccionado?",
                    title: "Confirmación",
                    buttons: {
                        yes: {
                            label: "Sí",
                            className: "green",
                            callback: function() {
                                $.get($url + "/delete/"+data.id, function(response) {
                                    if(response.success==true && response.id==-1)
                                    {
                                        toastr.options = {
                                            "newestOnTop": true,
                                            "positionClass": "toast-bottom-right",
                                        }
                                        toastr.warning('Esta categoría se encuentra asociada a una pieza. Elimine la pieza y luego podrá eliminar la categoría.', 'Información');
                                    }else if(response.success==true && response.id==data.id)
                                    {
                                        $('table.dataTable').DataTable().clearPipeline().draw();
                                        toastr.options = {
                                            "newestOnTop": true,
                                            "positionClass": "toast-bottom-right",
                                        }
                                        toastr.success('Registro eliminado', 'Información');
                                    }
                                });
                            }
                        },
                        no: {
                            label: "No",
                            className: "red",
                            callback: function() {
                                console.log("no");
                            }
                        }
                    }
                });
            }
        });

        $('#dtTable').on('click', 'tr', function(event) {
            //
        }).on('dblclick', 'td', function(event) {
            var data = oTable.DataTable().row(this).data();
            editar(data);
        });
    }
    var initDataTable2 = function(){

        var table = $('#dtTablePedidos');

        var oTable = table.dataTable({
            destroy:true,
            "language": languageDataTablet,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  $url + "/obtenerProductos",// ajax source
                "dataSrc": "list"
            },

            "columns": [

                { "data": "id_producto", "title": "Id del Producto", "visible": false},
                { "data": "referencia", "title": "Referencia del Producto", "visible": false},
                { "data": "descripcion", "title": "Descripción del Producto","visible":true},
                { "data": "stockActualP", "title": "Stock Actual", "visible": true},
                { "data": "stockDisponibleP", "title": "Stock disponible", "visible": true},
                { "data": null, "title": "Stock disponible", "visible": true},
                { "data": null, "title": "Stock disponible", "visible": true},
                { "data": null, "title": "Precio", "visible": true,
                render: function (data, type, row) {
                    var input = '<input type="text" id="precio" class="form-control" style="width:100%; height:50%;">';
                return input;
                }
                 },
                { "data": null, "title": "Cantidad",  "width": "80px", "visible": true,
                render: function (data, type, row) {
                    var input = '<input type="text" id="cantidad" class="form-control" style="width:100%; height:50%;">';
                return input;
                }
                },
               
        {
           
            "data": null,
            "className" : "center",
            "width": "110px",
            "title":"Acción",
            "defaultContent": '<div class="text-center bt_dt">' +
            '<button type="button" class="btn btn-sm default blue modal-toggle" id="btAddProducto" title="Añadir">' +
            '<i class="fa fa-plus"></i>' +
            '</button>' +
            '</div>',
            "orderable": false
        }
        ], 
        columnDefs: [ {
        }],
        select: "single",
        buttons: [
        { extend: 'copy', className: 'btn red btn-outline' },
        { extend: 'excel', className: 'btn yellow btn-outline ' },
        { extend: 'csv', className: 'btn purple btn-outline ' },
        { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'},
        {
            text: 'Recargar',
            className: 'btn default',
            action: function ( e, dt, node, config ) {
                dt.clearPipeline().draw();
            }
        },
        ],
        responsive: false,
        "order": [
        ],
        "lengthMenu": [
        [5, 10, 15, 20, -1],
                [5, 10, 15, 20, 60] // change per page values here
                ],
                "pageLength": 5,
            });


        $('#table_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });

        $('#cliente_id').on('change',function(){
            idcliente = $('#cliente_id').val();
            console.log("Imprimiendo id del cliente::: " + idcliente);
         })

        $('#dtTablePedidos tbody').on('click', '#btAddProducto', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();
            if(this.id == "btAddProducto"){
                cliente_id = idcliente;
                stock = data.stockDisponibleP;
                idproducto = data.id_producto;
                precio = $('#precio').val();
                cantidad = $('#cantidad').val();
                total = precio * cantidad;
                if(cantidad == '' || cliente_id == null){
                    showAlert("Notificación","Seleccione un cliente y/o Ingrese una cantidad!!!");
                }
                else if(cantidad < stock){
                    console.log(idproducto + "/" + precio + "/" +  cantidad + "/" + total);
                    var datatable = $('#dtTableP').DataTable();
                    producto['dt'].push({   
                                            id:'-1',
                                            id_pedido:'',
                                            idproducto: idproducto,
                                            cliente_id : idcliente,
                                            descripcion:data.descripcion,
                                            precio:$('#precio').val(),
                                            cantidad:$('#cantidad').val(),
                                            tot:total
                                           });
                    datatable.clear().rows.add(producto['dt']).draw();
                 $('#cantidad,#precio').val('');
                }else{
                    showAlert("Notificación","La Cantidad solicitada excede al stock disponible de producto!!!");
                    $('#cantidad').val('');
                }
         
               
                console.log(producto['dt']);
            }
            if(this.id == "btEliminar"){
                bootbox.dialog({
                    message: "¿Está seguro de eliminar el registro seleccionado?",
                    title: "Confirmación",
                    buttons: {
                        yes: {
                            label: "Sí",
                            className: "green",
                            callback: function() {
                                $.get($url + "/delete/"+data.id, function(response) {
                                    if(response.success==true && response.id==-1)
                                    {
                                        toastr.options = {
                                            "newestOnTop": true,
                                            "positionClass": "toast-bottom-right",
                                        }
                                        toastr.warning('Esta categoría se encuentra asociada a una pieza. Elimine la pieza y luego podrá eliminar la categoría.', 'Información');
                                    }else if(response.success==true && response.id==data.id)
                                    {
                                        $('table.dataTable').DataTable().clearPipeline().draw();
                                        toastr.options = {
                                            "newestOnTop": true,
                                            "positionClass": "toast-bottom-right",
                                        }
                                        toastr.success('Registro eliminado', 'Información');
                                    }
                                });
                            }
                        },
                        no: {
                            label: "No",
                            className: "red",
                            callback: function() {
                                console.log("no");
                            }
                        }
                    }
                });
            }
        });

        $('#dtTablePedidos').on('click', 'tr', function(event) {
            //
        }).on('dblclick', 'td', function(event) {
            var data = oTable.DataTable().row(this).data();
            editar(data);
        });
    }

 
    var initDataTable3 = function(){

        var table = $('#dtTableP');

        var oTable2 = table.dataTable({
            destroy:true,
            "language": languageDataTablet,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  $url + "/getPedidos",// ajax source
                "dataSrc": ""
            },
            "columns": [

                { "data": "descripcion", "title": "Producto", "visible": true},
                { "data": "precio", "title": "Precio", "visible": true},
                { "data": "cantidad", "title": "Cantidad", "visible": true},
                { "data": "tot", "title": "Total", "visible": true},
        {
            "targets": -1,
            "data": null,
            "title": 'Acción',
            "className" : "center",
            "width": "110px",
            "defaultContent": '<div class="text-center bt_dt">' +
            '<button type="button" class="btn btn-sm default red modal-toggle" id="btEliminarP" title="Eliminar Producto">' +
            '<i class="fa fa-trash"></i>' +
            '</button>' +
            '</div>',
            "orderable": false
        }
        ],
        columnDefs: [ {
        }],
        select: "single",
        buttons: [
        { extend: 'copy', className: 'btn red btn-outline' },
        { extend: 'excel', className: 'btn yellow btn-outline ' },
        { extend: 'csv', className: 'btn purple btn-outline ' },
        { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'},
        {
            text: 'Recargar',
            className: 'btn default',
            action: function ( e, dt, node, config ) {
                dt.clearPipeline().draw();
            }
        },
        ],
        responsive: false,
        "order": [
        ],
        "lengthMenu": [
        [5, 10, 15, 20, -1],
                [5, 10, 15, 20, 60] // change per page values here
                ],
                "pageLength": 5,
            });


        $('#table_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable2.DataTable().button(action).trigger();
        });

        $('#dtTableP tbody').on('click', ' #btEliminarP', function() {
            var data = oTable2.DataTable().row($(this).parents('tr')).data();
      
            if(this.id == "btEliminarP"){
                var table = $('#dtTableP').dataTable().api();
                var indice = oTable2.DataTable().row($(this).parents('tr')).index();
                if (data.id == '-1') {
                    bootbox.dialog({
                    message: "Está seguro de eliminar el registro seleccionado?",
                    title: "Confirmación",
                    buttons: {
                              yes: {
                                  label: "Si",
                                  className: "green",
                                  callback: function() {
                                      var max = producto['dt'].length -1;
                                      var to = indice < max ? (indice + 1) : indice;  
                                      var temp = producto['dt'].splice(indice, 1)[0];  
                                      table.clear().rows.add(producto['dt']).draw();
                                  }
                              },
                              no: {
                                  label: "No",
                                  className: "red",
                                  callback: function() {
                                  }
                              }
                          }
                      });  
                }
            }//fin de eliminar
        });

        $('#dtTableP').on('click', 'tr', function(event) {
            //
        }).on('dblclick', 'td', function(event) {
            var data = oTable.DataTable().row(this).data();
            editar(data);
        });
    }

    $('#cliente_id').on('change',function(){
        var idcliente = $('#cliente_id').val();
        $.get($url + "/datosClientes?idcliente=" + idcliente, function(response) {
            $('#rif').val(response['rif']);
            $('#direccion_fiscal').val(response['direccion_fiscal']);
            $('#direccion_entrega').val(response['direccion_entrega']);
        });
    });

    $('.Element').on('click',function(){
        producto = {dt:[]}; //blanqueo el arreglo para que no se vean los datos anteriores
        $('#dtTablePedidos').empty();
      
        initDataTable2(); 
        initDataTable3();
        $('#btGuardarPedido').removeClass('hide');
        $('#tablePrincipal').addClass('hide');
        $('#tablasecundaria').removeClass('hide');
     
        $('#rif,#direccion_fiscal,#direccion_entrega').val('');
        $('#cliente_id').val('null').trigger('change');
    });

    $('#btGuardarPedido').on('click', function(){
    
        var producto = $.map($('#dtTableP').DataTable().rows().data(), function (item) {
            return item;
            });
           
        var url = $url + '/savePedido';
            if(producto.length > 0){
              $.ajax({
                   url: url,
                   type: 'post',
                   data: {
                   producto: JSON.stringify(producto)
                   },
                   beforeSubmit: function (formData, jqForm, options) {
                       console.log("beforeSubmit");
                    
                   },
                   success: function (responseText, statusText, xhr, $form) {

                      $('#dtTableP').dataTable().api().ajax.reload();
                      showAlert("Guardar Pedido","Pedido guardado con Éxito!!!");
                      $('#btGuardarPedido').addClass('hide');
                      initDataTable();
                      $('#tablePrincipal').removeClass('hide');
                      $('#tablasecundaria').addClass('hide');
                     
                   }
                
               });
            }else{
                showAlert("Registrar Productos", "Debe ingresar un producto");
            } 
         
    });



    return {

        init: function () {

            initDataTable();
            setJQueryValidate();

        }
    };
}();

jQuery(document).ready(function() {
    productos.init();

   /*  $('#btnAgregar').on('click', function (e) {
        formReset($('#formCRUD'));
    });

    $('#btCancelar').on('click', function (e) {
        formReset($('#formCRUD'));
        $('#mCRUD').modal('toggle');
    });

    $('#btCancelar2').on('click', function (e) {
        formReset($('#formCRUDEdit'));
        $('#mCRUDEdit').modal('toggle');
    }); */
});