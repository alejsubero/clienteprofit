var usuarios = function () {

    var formCRUD = $('#formCRUD');

    var setJQueryValidate = function(){
        formCRUD.validate({
            doNotHideMessage: true,
            errorElement: 'span',
            errorClass: 'help-block help-block-error',
            focusInvalid: true,
            ignoreTitle: true,
            messages : {
                required: "Este campo es requerido.",
                number : "Por favor ingrese solo números."
            },
            rules : {

            },
            invalidHandler : function (event, validator) {
            },
            errorPlacement: function (error, element) {
                var cont = $(element).parent('.input-group');

                error.appendTo(element.closest(".form-group"));
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (element) {
                element.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {

            },
            color: {
                required: true,
                minlength: 7,
            }
        });

        //formCRUD.validate();
    }

    var initDataTable = function(){

        var table = $('#dtTable');

        var oTable = table.dataTable({
            //destroy:true,
            "language": languageDataTablet,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  $url + "/get",// ajax source
                "dataSrc": "list"
            },
            "createdRow": function ( row, data, index ) {
            },
            "columns": [
            { "data": "id", "title": "id", "visible": false, "orderable": true},
            { "data": "nombres", "title": "Nombres", "visible": true, "orderable": true },
            { "data": "apellidos", "title": "Apellidos", "visible": true, "orderable": true },
            { "data": "email", "title": "Email", "visible": true, "orderable": true },
           

        {
            "targets": -1,
            "data": null,
            "title": "Actión",
            "className" : "",
            "width": "110px",
            "defaultContent": '<div class="text-center bt_dt">' +
            '<button style="font-size:12px;width:38%;padding:5px;" type="button" class="btn btn-success" id="btEditar" title="Modificar">Edit ' +
            '<i class="fa fa-edit"></i>' +
            '</button>' +
            '<button style="font-size:12px;width:45%;padding:5px;" type="button" class="btn btn-danger" id="btEliminar" title="Baja">Delete ' +
            '<i class="fa fa-trash"></i>' +
            '</button>' +
            '</div>',
            "orderable": false
        }
        ],
        columnDefs: [ {
        }],
        select: "single",
        buttons: [
        { extend: 'copy', className: 'btn red btn-outline' },
        { extend: 'excel', className: 'btn yellow btn-outline ' },
        { extend: 'csv', className: 'btn purple btn-outline ' },
        { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'},
        {
            text: 'Recargar',
            className: 'btn default',
            action: function ( e, dt, node, config ) {
                dt.clearPipeline().draw();
            }
        },
        ],
        responsive: false,
        "order": [
        ],
        "lengthMenu": [
        [5, 10, 15, 20, -1],
                [5, 10, 15, 20, 60] // change per page values here
                ],
                "pageLength": 30,
            });

       
        $('#table_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });

        $('#dtTable tbody').on('click', '#btEditar, #btEliminar', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();
            if(this.id == "btEditar"){

                editar(data);

            }
            if(this.id == "btEliminar"){
                bootbox.dialog({
                    message: "Está seguro de eliminar el registro seleccionado?",
                    title: "Confirmación",
                    buttons: {
                        yes: {
                            label: "Si",
                            className: "green",
                            callback: function() {
                                $.get($url + "/delete/"+data.id, function(response) {
                                    $('table.dataTable').DataTable().clearPipeline().draw();
                                });
                            }
                        },
                        no: {
                            label: "No",
                            className: "red",
                            callback: function() {
                                console.log("no");
                            }
                        }
                    }
                });
            }
        });

        $('#dtTable').on('click', 'tr', function(event) {
            //
        }).on('dblclick', 'td', function(event) {
            var data = oTable.DataTable().row(this).data();
            editar(data);
        });
    }

    $('#btGuardar').on('click', function (e) {
        guardar();
    });

    var editar = function(data){
        $.get($url + "/get/"+data.id, function(response) {
            PopulateForm("#formCRUD", response.data[0]);
            $('#mCRUD').modal('toggle');
        });
    }
    var guardar = function () {
        if (formCRUD.valid()) {
            var id = $("#id").val();
            var url = $url + '/save';
            $("#formCRUD").ajaxSubmit({
                url: url,
                type: 'post',
                beforeSubmit: function (formData, jqForm, options) {
                },
                success: function (responseText, statusText, xhr, $form) {
                    console.log(responseText);
                    var table = $('#dtTable').dataTable().api();

                    if(responseText.success){
                        bootbox.alert({
                        size: "",
                        title: "Guardar Registro",
                        message: "Registro Procesado con Exito!!!",
                        callback: function () {
                             table.ajax.reload();
                           

                        }
                    });

                    }else{

                    }
                            table.ajax.reload();
                    $('#mCRUD').modal('toggle');
                }
            });

        } else {
            console.log("No valid");
            return false;
        }
    }

    $('#btUpdatePassword').on('click',function(){
        UpdatePassword();
    });

    //FUNCION PARA ACTUALIZAR PASSWORD
    var UpdatePassword = function () {
        if (formCRUD.valid()) {
            var id = $("#id").val();
            var url = $urlBase + '/usuarios/updatepassword';
            $("#formCRUD").ajaxSubmit({
                url: url,
                type: 'post',
                beforeSubmit: function (formData, jqForm, options) {},
                success: function (responseText, statusText, xhr, $form) {
                    console.log(responseText);
                    if (responseText.success) {
                        bootbox.alert({
                            size: "",
                            title: "Actualizar Contraseña",
                            message: "Contraseña Actualizada con Exito!!!",
                            callback: function () {
                                $('#password,#password_confirmation').val('');
                            }
                        });
                    } else {
                        bootbox.alert({
                            size: "",
                            title: "Actualizar Contraseña",
                            message: "Error al Actualizar la Contraseña!!!",
                            callback: function () {

                            }
                        });

                    }

                }
            });

        } else {
            console.log("No valid");
            return false;
        }
    }
    //FIN

    return {

        init: function () {
            initDataTable();
            setJQueryValidate();

            fillComboBox($urlBase + '/roles/getComboRoles', $("#role_id"));
            //fillComboBox3($urlBase + '/usuarios/getComboPrograma', $("#programa_id"));

        }
    };
}();

jQuery(document).ready(function() {
    usuarios.init();

    $('#btnAgregar').on('click', function (e) {
        formReset($('#formCRUD'));
    });

    $('#btCancelar').on('click', function (e) {
        formReset($('#formCRUD'));
        $('#mCRUD').modal('toggle');
    });

    $('#color').colorpicker();
});