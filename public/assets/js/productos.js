var productos = function () {

    var formCRUD = $('#formCRUDProductos');

    var setJQueryValidate = function(){
        //jQuery.validator.setDefaults({
        formCRUD.validate({
            doNotHideMessage: true,
            errorElement: 'span',
            errorClass: 'help-block help-block-error',
            focusInvalid: true,
            ignoreTitle: true,
            //ignore: '',
            messages : {
                required: "Este campo es requerido.",
                number : "Por favor ingrese solo números."
            },
            rules : {

            },
            invalidHandler : function (event, validator) {

            },
            errorPlacement: function (error, element) {
                var cont = $(element).parent('.input-group');

                error.appendTo(element.closest(".form-group"));
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (element) {
                element.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {

            }
        });
    }
     var initDataTable = function(){

        var table = $('#dtTable');

        var oTable = table.dataTable({
            destroy:true,
            "language": languageDataTablet,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  $url + "/getProductosall",// ajax source
                "dataSrc": "list"
            },

            "columns": [

                { "data": "descripcion", "title": "Nombre Producto", "visible": true},
                { "data": "cantidad", "title": "Cantidad", "visible":true},
                { "data": "tot", "title": "Total", "visible": true},
            
        {
            "targets": -1,
            "data": null,
            "className" : "center",
            "width": "110px",
            "title":"Acción",
            "defaultContent": '<div class="text-center bt_dt">' +
            '<button type="button" class="btn btn-sm default blue modal-toggle" id="btEditar" title="Modificar">' +
            '<i class="fa fa-edit"></i>' +
            '</button>' +
            '<button type="button" class="btn btn-sm default red modal-toggle" id="btEliminar" title="Baja">' +
            '<i class="fa fa-close"></i>' +
            '</button>' +
            '</div>',
            "orderable": false
        }
        ],
        columnDefs: [ {
        }],
        select: "single",
        buttons: [
        { extend: 'copy', className: 'btn red btn-outline' },
        { extend: 'excel', className: 'btn yellow btn-outline ' },
        { extend: 'csv', className: 'btn purple btn-outline ' },
        { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'},
        {
            text: 'Recargar',
            className: 'btn default',
            action: function ( e, dt, node, config ) {
                dt.clearPipeline().draw();
            }
        },
        ],
        responsive: false,
        "order": [
        ],
        "lengthMenu": [
        [5, 10, 15, 20, -1],
                [5, 10, 15, 20, 60] // change per page values here
                ],
                "pageLength": 5,
            });


        $('#table_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });

        $('#dtTable tbody').on('click', '#btEditar, #btEliminar', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();
            if(this.id == "btEditar"){

                editar(data);

            }
            if(this.id == "btEliminar"){
                bootbox.dialog({
                    message: "¿Está seguro de eliminar el registro seleccionado?",
                    title: "Confirmación",
                    buttons: {
                        yes: {
                            label: "Sí",
                            className: "green",
                            callback: function() {
                                $.get($url + "/delete/"+data.id, function(response) {
                                    if(response.success==true && response.id==-1)
                                    {
                                        toastr.options = {
                                            "newestOnTop": true,
                                            "positionClass": "toast-bottom-right",
                                        }
                                        toastr.warning('Esta categoría se encuentra asociada a una pieza. Elimine la pieza y luego podrá eliminar la categoría.', 'Información');
                                    }else if(response.success==true && response.id==data.id)
                                    {
                                        $('table.dataTable').DataTable().clearPipeline().draw();
                                        toastr.options = {
                                            "newestOnTop": true,
                                            "positionClass": "toast-bottom-right",
                                        }
                                        toastr.success('Registro eliminado', 'Información');
                                    }
                                });
                            }
                        },
                        no: {
                            label: "No",
                            className: "red",
                            callback: function() {
                                console.log("no");
                            }
                        }
                    }
                });
            }
        });

        $('#dtTable').on('click', 'tr', function(event) {
            //
        }).on('dblclick', 'td', function(event) {
            var data = oTable.DataTable().row(this).data();
            editar(data);
        });
    }

    $('#producto_id').on('change', function(){
        id_producto = $('#producto_id').val();
        console.log(id_producto);
    })
    $('#addimagen').on('click', function (e) {
        guardarImagen();
    });

    var guardarImagen = function () {
        if (formCRUD.valid()) {
            var url = $url + '/saveImagenProducto';
            $("#formCRUDProductos").ajaxSubmit({
                url: url,
                type: 'post',
                beforeSubmit: function (formData, jqForm, options) {
                },
                success: function (responseText, statusText, xhr, $form) {
                    console.log("Por Aquiiii Estoy::: " + responseText.success);
               
                    if(responseText){
                        bootbox.alert({
                        size: "",
                        title: "Guardar Registro de Imagen",
                        message: "Imagen Guardada con Exito!!!",
                        callback: function () {
                        
                        }
                    });

                    }
                }
            });

        } 
    }

    return {

        init: function () {

            initDataTable();
            setJQueryValidate();

        }
    };
}();

jQuery(document).ready(function() {
    productos.init();

    $("#logo").fileinput({
        showPreview: true,
        showUpload: false,
        elErrorContainer: '#kartik-file-errors',
        allowedFileExtensions: ["jpg", "png", "gif", "svg", "ico"]

    });
});