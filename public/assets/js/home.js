var home = function () {


//GRÁFICA GENERAL    
AmCharts.makeChart("chartdiv",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "angle": 30,
                    "depth3D": 30,
                    "startDuration": 1,
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "alphaField": "column-1",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                            "fillAlphas": 1,
                            "id": "AmGraph-1",
                            "title": "voluntariado",
                            "type": "column",
                            "valueField": "column-1"
                        },
                        {
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                            "fillAlphas": 1,
                            "id": "AmGraph-2",
                            "title": "Construcción y Reconstrucción",
                            "type": "column",
                            "valueField": "column-2"
                        },
                        {
                            "alphaField": "column-3",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                             "fillAlphas": 1,
                            "id": "AmGraph-3",
                            "title": "Quiero Saber",
                            "type": "column",
                            "valueField": "column-3",
                            "xAxis": "ValueAxis-1"
                        },

                        {
                            "alphaField": "column-4",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                             "fillAlphas": 1,
                            "id": "AmGraph-4",
                            "title": "TecnoFuturos",
                            "type": "column",
                            "valueField": "column-4"
                        },
                        {
                            "alphaField": "column-5",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                             "fillAlphas": 1,
                            "id": "AmGraph-5",
                            "title": "Emprendimiento",
                            "type": "column",
                            "valueField": "column-5"
                        },
                        {
                            "alphaField": "column-6",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                             "fillAlphas": 1,
                            "id": "AmGraph-6",
                            "title": "Punto y Seguimos",
                            "type": "column",
                            "valueField": "column-6"
                        },
                        {
                            "alphaField": "column-7",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                             "fillAlphas": 1,
                            "id": "AmGraph-7",
                            "title": "Alimentes",
                            "type": "column",
                            "valueField": "column-7"

                        }      
                           ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "axisTitleOffset": 11,
                            "id": "ValueAxis-1",
                            "position": "bottom",
                            "offset": 3,
                            "title": "Atendidos",
                            "titleRotation": -90
                        },
                        {
                            "id": "ValueAxis-2",
                            "minHorizontalGap": 61
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": true,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 15,
                            "text": "Crecimento por Programas desde 2009 / 2018"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "2009",
                           // "column-1": 542,
                            //"column-2": 5,
                            //"column-3": 9,
                            //"column-4": 14,
                            "column-5": 65,
                            //"column-6": 40,
                           // "column-7": 20
                        },
                        {
                            "category": "2010",
                           // "column-1": 6,
                            "column-2": 7,
                            //"column-3": 10,
                           // "column-4": 48,
                            "column-5": 105,
                            "column-6": 317,
                            //"column-7": 20
                        },
                        {
                            "category": "2011",
                            //"column-1": "20",
                            "column-2": 3,
                            "column-3": 888,
                            "column-4": 1660,
                            "column-5": 209,
                            "column-6": 308,
                            //"column-7": 1660
                        },
                        {
                            "category": "2012",
                           // "column-1": 5,
                            "column-2": 8,
                            "column-3": 1249,
                            "column-4": 1710,
                            "column-5": 436,
                            "column-6": 315,
                            //"column-7": 1710
                        },
                        {
                            "category": "2013",
                           // "column-1": 20,
                            "column-2": 20,
                            "column-3": 1886,
                            "column-4": 1767,
                            "column-5": 271,
                            "column-6": 315,
                            //"column-7": 1767
                        },
                        {
                            "category": "2014",
                           // "column-1": 20,
                            "column-2": 20,
                            "column-3": 1574,
                            "column-4": 1893,
                            "column-5": 154,
                            "column-6": 317,
                            //"column-7": 1893
                        },
                        {
                            "category": "2015",
                           // "column-1": 20,
                            "column-2": 20,
                            "column-3": 1482,
                            "column-4": 1720,
                            "column-5": 379,
                            "column-6": 225,
                            //"column-7": 1720
                        },
                        {
                            "category": "2016",
                            "column-1": 162,
                            "column-2": 20,
                            "column-3": 1884,
                            "column-4": 602,
                            "column-5": 465,
                            "column-6": 339,
                           // "column-7": 602
                        },
                        {
                            "category": "2017",
                            "column-1": 162,
                            "column-2": 20,
                            "column-3": 1884,
                            "column-4": 602,
                            "column-5": 178,
                            "column-6": 339,
                            "column-7": 2931
                        },
                        {
                            "category": "2018",

                        }
                    ]
                }
            );
//GRÁFICA VOLUNTARIADO
AmCharts.makeChart("chartdiv2",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "angle": 30,
                    "depth3D": 5,
                    "startDuration": 1,
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "alphaField": "column-1",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                            "fillAlphas": 1,
                            "id": "AmGraph-1",
                            "title": "Voluntariado",
                            "type": "column",
                            "valueField": "column-1"
                        }
      
                           ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "axisTitleOffset": 11,
                            "id": "ValueAxis-1",
                            "position": "bottom",
                            "offset": 3,
                            "title": "Atendidos",
                            "titleRotation": -90
                        },
                        {
                            "id": "ValueAxis-2",
                            "minHorizontalGap": 61
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": true,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 15,
                            "text": " Programa Voluntariado 2009-2018"
                        }
                    ],
                    "dataProvider": [

                        {
                            "category": "2016",
                            "column-1": 112
                        },
                        {
                            "category": "2017",
                            "column-1": 162
                        },
                        {
                            "category": "2018",
                            "column-1": 20

                        }
                    ]
                }
            );
//FIN GRÁFICA VOLUNTARIADO
/*-----------------------------------------------------------------------------------*/
//GRÁFICA QUIERO SABER
AmCharts.makeChart("quierosaber",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "angle": 30,
                    "depth3D": 5,
                    "colors": [
                        "#B0DE09"
                    ],
                    "startDuration": 1,
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "alphaField": "column-2",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                            "fillAlphas": 1,
                            "id": "AmGraph-1",
                            "title": "Quiero Saber",
                            "type": "column",
                            "valueField": "column-2"
                        }
      
                           ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "axisTitleOffset": 11,
                            "id": "ValueAxis-1",
                            "position": "bottom",
                            "offset": 3,
                            "title": "Atendidos",
                            "titleRotation": -90
                        },
                        {
                            "id": "ValueAxis-2",
                            "minHorizontalGap": 61
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": true,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 15,
                            "text": " Programa Quiero Saber 2011-2018"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "2011",
                            "column-2": "888"
                        },
                        {
                            "category": "2012",
                            "column-2": "1249"
                        },
                        {
                            "category": "2013",
                            "column-2": "1886"
                        },
                        {
                            "category": "2014",
                            "column-2": "1574"
                        },
                        {
                            "category": "2015",
                            "column-2": "1482"
                        },
                        {
                            "category": "2016",
                            "column-2": "1884"
                        },
                        {
                            "category": "2017",
                            "column-2": "1884"
                        },
                        {
                            "category": "2018",
                            "column-2": null
                        }
                    ]
                }
            );

//FIN GRÁFICA QUIERO SABER
/*-----------------------------------------------------------------------------------------*/
//GRÁFICA TECNOFUTUROS
AmCharts.makeChart("tecnofuturos",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "angle": 30,
                    "depth3D": 5,
                    "colors": [
                        "#0a72a6"
                    ],
                    "startDuration": 1,
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "alphaField": "column-2",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                            "fillAlphas": 1,
                            "id": "AmGraph-1",
                            "title": "TecnoFuturos",
                            "type": "column",
                            "valueField": "column-2"
                        }
      
                           ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "axisTitleOffset": 11,
                            "id": "ValueAxis-1",
                            "position": "bottom",
                            "offset": 3,
                            "title": "Atendidos",
                            "titleRotation": -90
                        },
                        {
                            "id": "ValueAxis-2",
                            "minHorizontalGap": 61
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": true,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 15,
                            "text": " Programa TecnoFuturos 2011-2018"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "2011",
                            "column-2": "1660"
                        },
                        {
                            "category": "2012",
                            "column-2": "1710"
                        },
                        {
                            "category": "2013",
                            "column-2": "1767"
                        },
                        {
                            "category": "2014",
                            "column-2": "1893"
                        },
                        {
                            "category": "2015",
                            "column-2": "1720"
                        },
                        {
                            "category": "2016",
                            "column-2": "602"
                        },
                        {
                            "category": "2017",
                            "column-2": "602"
                        },
                        {
                            "category": "2018",
                            "column-2": null
                        }
                    ]
                }
            );
//FIN GRÁFICA TECNOFUTUROS
/*----------------------------------------------------------------------------------------*/

//GRÁFICA PUNTO Y SEGUIMOS
AmCharts.makeChart("puntoyseguimos",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "angle": 30,
                    "depth3D": 5,
                    "colors": [
                        "#CD0D74"
                    ],
                    "startDuration": 1,
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "alphaField": "column-2",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                            "fillAlphas": 1,
                            "id": "AmGraph-1",
                            "title": "Punto y Seguimos",
                            "type": "column",
                            "valueField": "column-2"
                        }
      
                           ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "axisTitleOffset": 11,
                            "id": "ValueAxis-1",
                            "position": "bottom",
                            "offset": 3,
                            "title": "Atendidos",
                            "titleRotation": -90
                        },
                        {
                            "id": "ValueAxis-2",
                            "minHorizontalGap": 61
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": true,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 15,
                            "text": " Programa Punto y Seguimos 2010-2018"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "2010",
                            "column-2": "6937"
                        },
                        {
                            "category": "2011",
                            "column-2": "7265"
                        },
                        {
                            "category": "2012",
                            "column-2": "6956"
                        },
                        {
                            "category": "2013",
                            "column-2": "6387"
                        },
                        {
                            "category": "2014",
                            "column-2": "6579"
                        },
                        {
                            "category": "2015",
                            "column-2": "3454"
                        },
                        {
                            "category": "2016",
                            "column-2": "7515"
                        },
                        {
                            "category": "2017",
                            "column-2": "6100"
                        },
                        {
                            "category": "2018",
                            "column-2": null
                        }
                    ]
                }
            );
//FIN GRÁFICA PUNTO Y SEGUIMOS
/*----------------------------------------------------------------------------------------*/

//GRÁFICA EMPRENDIMIENTO
AmCharts.makeChart("emprendimiento",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "angle": 30,
                    "depth3D": 5,
                    "colors": [
                        "#2A0CD0"
                    ],
                    "startDuration": 1,
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "alphaField": "column-2",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                            "fillAlphas": 1,
                            "id": "AmGraph-1",
                            "title": "Emprendimiento",
                            "type": "column",
                            "valueField": "column-2"
                        }
      
                           ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "axisTitleOffset": 11,
                            "id": "ValueAxis-1",
                            "position": "bottom",
                            "offset": 3,
                            "title": "Atendidos",
                            "titleRotation": -90
                        },
                        {
                            "id": "ValueAxis-2",
                            "minHorizontalGap": 61
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": true,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 15,
                            "text": " Programa Emprendimiento 2010-2018"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "2009",
                            "column-2": "65"
                        },
                        {
                            "category": "2010",
                            "column-2": "105"
                        },
                        {
                            "category": "2011",
                            "column-2": "209"
                        },
                        {
                            "category": "2012",
                            "column-2": "436"
                        },
                        {
                            "category": "2013",
                            "column-2": "271"
                        },
                        {
                            "category": "2014",
                            "column-2": "154"
                        },
                        {
                            "category": "2015",
                            "column-2": "3454"
                        },
                        {
                            "category": "2016",
                            "column-2": "7515"
                        },
                        {
                            "category": "2017",
                            "column-2": "6100"
                        },
                        {
                            "category": "2018",
                            "column-2": null
                        }
                    ]
                }
            );
//FIN GRÁFICA EMPRENDIMIENTO
/*----------------------------------------------------------------------------------------*/

//GRÁFICA ALIMENTES
AmCharts.makeChart("alimentes",
                {
                    "type": "serial",
                    "categoryField": "category",
                    "angle": 30,
                    "depth3D": 5,
                    "colors": [
                        "#CC0000"
                    ],
                    "startDuration": 1,
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "trendLines": [],
                    "graphs": [
                        {
                            "alphaField": "column-2",
                            "balloonText": "[[title]]  [[category]]:[[value]]",
                            "fillAlphas": 1,
                            "id": "AmGraph-1",
                            "title": "Alimentes",
                            "type": "column",
                            "valueField": "column-2"
                        }
      
                           ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "axisTitleOffset": 11,
                            "id": "ValueAxis-1",
                            "position": "bottom",
                            "offset": 3,
                            "title": "Atendidos",
                            "titleRotation": -90
                        },
                        {
                            "id": "ValueAxis-2",
                            "minHorizontalGap": 61
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "legend": {
                        "enabled": true,
                        "useGraphSettings": true
                    },
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 15,
                            "text": " Programa Alimentes 2017-2018"
                        }
                    ],
                    "dataProvider": [
                        {
                            "category": "2017",
                            "column-2": "2931"
                        },
                        {
                            "category": "2018",
                            "column-2": null
                        }
                    ]
                }
            );
//FIN GRÁFICA ALIMENTES
/*----------------------------------------------------------------------------------------*/
    return {

        init: function () {

        }
    };

}();

jQuery(document).ready(function() {
    home.init();
});

