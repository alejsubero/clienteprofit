var clientes = function () {

    var formCRUD = $('#formCRUD');

    var setJQueryValidate = function(){
        //jQuery.validator.setDefaults({
        formCRUD.validate({
            doNotHideMessage: true,
            errorElement: 'span',
            errorClass: 'help-block help-block-error',
            focusInvalid: true,
            ignoreTitle: true,
            //ignore: '',
            messages : {
                required: "Este campo es requerido.",
                number : "Por favor ingrese solo números."
            },
            rules : {

            },
            invalidHandler : function (event, validator) {
                //$('.alert-error', $('.login-form')).show();
            },
            errorPlacement: function (error, element) {
                var cont = $(element).parent('.input-group');

                /* if (cont) {
                    console.log(cont);
                    cont.after(error);
                } else {
                    console.log(element);
                    //element.after(error);
                    error.appendTo(element.closest(".form-group"));
                }*/
                error.appendTo(element.closest(".form-group"));
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (element) {
                element.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {

            }
        });

        //formCRUD.validate();
    }

     var initDataTable = function(){

        var table = $('#dtTable');

        var oTable = table.dataTable({
            //destroy:true,
            "language": languageDataTablet,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  $url + "/getClientes",// ajax source
                "dataSrc": "list"


            },
            "createdRow": function ( row, data, index ) {
                if (data.estado == 1) {
                    //$(row).addClass('un-read');
                }else{
                    $(row).addClass('read');
                }
            },
            "columns": [

                { "data": "razon_social", "title": "Nombre Empresa", "visible": true},
                { "data": "email", "title": "Email", "visible":true},
                { "data": "direccion_entrega", "title": "Dirección de Entrega", "visible": true},
                { "data": "contacto", "title": "Persona Contacto", "visible": true},
                { "data": "telefono", "title": "Teléfonos", "visible": true},
        {
            "targets": -1,
            "title": "Action",
            "data": null,
            "className" : "",
            "width": "110px",
            "defaultContent": '<div class="text-center bt_dt">' +
            '<button style="font-size:12px;width:38%;padding:5px;" type="button" class="btn btn-success" id="btEditar" title="Modificar">Edit ' +
            '<i class="fa fa-edit"></i>' +
            '</button>' +
            '<button style="font-size:12px;width:45%;padding:5px;" type="button" class="btn btn-danger" id="btEliminar" title="Baja">Delete ' +
            '<i class="fa fa-trash"></i>' +
            '</button>' +
            '</div>',
            "orderable": false
        }
        ],
        columnDefs: [ {
        }],
        select: "single",
        buttons: [
        { extend: 'copy', className: 'btn red btn-outline' },
        { extend: 'excel', className: 'btn yellow btn-outline ' },
        { extend: 'csv', className: 'btn purple btn-outline ' },
        { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'},
        {
            text: 'Recargar',
            className: 'btn default',
            action: function ( e, dt, node, config ) {
                dt.clearPipeline().draw();
            }
        },
        ],
        responsive: false,
        "order": [
        ],
        "lengthMenu": [
        [5, 10, 15, 20, -1],
                [5, 10, 15, 20, 60] // change per page values here
                ],
                "pageLength": 5,
            });


        $('#table_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });

        $('#dtTable tbody').on('click', '#btEditar, #btEliminar', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();
            if(this.id == "btEditar"){

                editar(data);

            }
    if(this.id == "btEliminar"){

                bootbox.dialog({
                    message: "Está seguro de eliminar el registro seleccionado?",
                    title: "Confirmación",
                    buttons: {
                        yes: {
                            label: "Si",
                            className: "green",
                            callback: function() {
                              $.post($url + "/delete?id_cliente="+data.id_cliente, function(response) {

                                    oTable.DataTable().ajax.reload();
                                });
                            }
                        },
                        no: {
                            label: "No",
                            className: "red",
                            callback: function() {
                            }
                        }
                    }
                });

            }
        });

        $('#dtTable').on('click', 'tr', function(event) {
            //
        }).on('dblclick', 'td', function(event) {
            var data = oTable.DataTable().row(this).data();
            editar(data);
        });
    }



   $('#btGuardar').on('click', function (e) {
        guardarClientes();
    });
    $('#btActualizar').on('click', function (e) {
        actualizarClientes();
    });

  var editar = function(data){
      $('#btGuardar').addClass('hide');
      $('#btActualizar').removeClass('hide');
        $.get($url + "/getClientes?id_cliente=" + data.id_cliente, function (response) {
            console.log("Imprimiendo aquiiiii:::  " + response.list['porce_descuento_global']);
            PopulateForm("#formCRUD", response.list);
            $('#zona_id').val(response.list['id_zona']).trigger('change');
            $('#tipo_cliente_id').val(response.list['id_tipo_cliente']).trigger('change');
            $('#segmento_id').val(response.list['id_segmento']).trigger('change');
            $('#listaprecio_id').val(response.list['id_listaprecio']).trigger('change');
            $('#condicion_de_pago_id').val(response.list['id_condicion_de_pago']).trigger('change');
            $('#status_id').val(response.list['status']).trigger('change');
            $('#porcentaje').val(response.list['porce_descuento_global']);
            $('#mCRUD').modal('toggle');
        });
    }


     var guardarClientes = function () {
        if (formCRUD.valid()) {
            var id = $("#id").val();
            var url = $url + '/saveClientes';
            $("#formCRUD").ajaxSubmit({
                url: url,
                type: 'post',
                beforeSubmit: function (formData, jqForm, options) {
                },
                success: function (responseText, statusText, xhr, $form) {
                   
                    console.log("Por Aquiiii Estoy::: " + responseText.success);
                    var table = $('#dtTable').dataTable().api();

                    if(responseText){
                        bootbox.alert({
                        size: "",
                        title: "Guardar Registro",
                        message: "Registro Procesado con Exito!!!",
                        callback: function () {
                            table.ajax.reload();

                        }
                    });

                    }else{

                    }
                            table.ajax.reload();
                    $('#mCRUD').modal('toggle');
                }
            });

        } else {
            console.log("No valid");
            return false;
        }
    }

    var actualizarClientes = function(){
        if (formCRUD.valid()) {
            var id = $("#id").val();
            var url = $url + '/updateClientes';
            $("#formCRUD").ajaxSubmit({
                url: url,
                type: 'put',
                beforeSubmit: function (formData, jqForm, options) {
                },
                success: function (responseText, statusText, xhr, $form) {
                   
                    var table = $('#dtTable').dataTable().api();

                    if(responseText){
                        bootbox.alert({
                        size: "",
                        title: "Guardar Registro",
                        message: "Registro Actualizado con Exito!!!",
                        callback: function () {
                            table.ajax.reload();

                        }
                    });

                    }else{

                    }
                            table.ajax.reload();
                    $('#mCRUD').modal('toggle');
                }
            });

        } else {
            console.log("No valid");
            return false;
        }
    }

    return {

        init: function () {
            initDataTable();
            setJQueryValidate();

        }
    };
}();

jQuery(document).ready(function() {
    clientes.init();
    $('.config').on('click', function (e) {
        var zona = $('#zona_id').val();
        console.log(zona);
        if($Auth != 1){
        $.ajax({
            dataType: "json",
            url: $url + "/getConfiguracion",
            success: function(data) {
                console.log(data);
                for(var i = 0; i < data.configUser.length; i++){
                    var zona = data.configUser[i].id_zona;
                    var segmento = data.configUser[i].id_segmento;
                    var tipocliente = data.configUser[i].id_tipo_cliente;
                    var vendedor = data.configUser[i].id_vendedor;
                }
                if($Auth != 1){
                    $('#zona_id').empty();
                    $("#zona_id").append('<option value="">Seleccione...</option>');
                    for (var i = 0; i < data.zona.length; i++) {
                        if(data.zona[i].id_zona == zona){
                            
                            $("#zona_id").append('<option value="' + zona + '">' + data.zona[i].descripcion + '</option>');
                     
                        }
              
                    }

                    //Tipo cliente
                    $('#tipo_cliente_id').empty();
                    $("#tipo_cliente_id").append('<option value="">Seleccione...</option>');
                    for (var i = 0; i < data.tipocliente.length; i++) {
                        if(data.tipocliente[i].id_tipo_cliente == tipocliente){
                            $("#tipo_cliente_id").append('<option value="' + tipocliente + '">' + data.tipocliente[i].descripcion + '</option>');
                        }
                    
                    }

                    //segmento
                    $('#segmento_id').empty();
                    $("#segmento_id").append('<option value="">Seleccione...</option>');
                    for (var i = 0; i < data.segmento.length; i++) {
                        if(data.segmento[i].id_segmento == segmento){
                            $("#segmento_id").append('<option value="' + segmento + '">' + data.segmento[i].descripcion + '</option>');
                        }
                   
                    }

                    $('#vendedor_id').empty();
                    $("#vendedor_id").append('<option value="">Seleccione...</option>');
                    for (var i = 0; i < data.vendedor.length; i++) {
                        if(data.vendedor[i].id_vendedor == vendedor){
                            $("#vendedor_id").append('<option value="' + vendedor + '">' + data.vendedor[i].nombre_completo + '</option>');
                        }
                   
                    }
             
                } 
            } //fin de success
        });    
    }
    });

     $('#btnAgregar').on('click', function (e) {
     
            formReset($('#formCRUD'));
            $('#btGuardar').removeClass('hide');
            $('#btActualizar').addClass('hide');
      
    }); 

    $('#btCancelar').on('click', function (e) {
        $('#rif').val('');
        $('#mCRUD').modal('toggle');
    });

    $('#btCancelar2').on('click', function (e) {
        formReset($('#formCRUDEdit'));
        $('#mCRUDEdit').modal('toggle');
    });
});
