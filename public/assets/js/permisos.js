var permisos = function () {

    var formCRUD = $('#formCRUD');

    var setJQueryValidate = function () {
        //jQuery.validator.setDefaults({
        formCRUD.validate({
            doNotHideMessage: true,
            errorElement: 'span',
            errorClass: 'help-block help-block-error',
            focusInvalid: true,
            ignoreTitle: true,
            //ignore: '',
            messages: {
                required: "Este campo es requerido.",
                number: "Por favor ingrese solo números."
            },
            rules: {

            },
            invalidHandler: function (event, validator) {
                //$('.alert-error', $('.login-form')).show();
            },
            errorPlacement: function (error, element) {
                var cont = $(element).parent('.input-group');

                error.appendTo(element.closest(".form-group"));
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (element) {
                element.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {

            }
        });

        //formCRUD.validate();
    }

    var initDataTable = function () {

        var table = $('#dtTable');

        var oTable = table.dataTable({
            dom: '<"row view-filter"<"col-sm-8"<"pull-left"f><"pull-left"<"toolbar">>><"col-sm-4"<"text-right"p>>>t<"row view-pager"<"col-sm-6"<"text-left"i>><"col-sm-6"<"text-right"p>>>',//'ftip',
            "language": languageDataTablet,
            fixedHeader: {
                header: true,
                headerOffset: $('.page-header').outerHeight(true)
            },
            "loadingMessage": 'Cargando...',
            "processing": true,
            "serverSide": true,
            "ajax": $.fn.dataTable.pipeline({
                url: $url + "/get",
                pages: 5 // number of pages to cache
            }),
            "createdRow": function (row, data, index) {
                //console.log(data.Hab);
                //if (data.Hab == 1) {

                //$(row).addClass('');
                //}else{
                //    $(row).addClass('desh');
                //}
            },
            "columns": [
                { "data": "id", "title": "id", "visible": false, "orderable": true },
                { "data": "name", "title": "Nombre", "visible": true, "orderable": true },
                { "data": "slug", "title": "Abrev.", "visible": true, "orderable": true },
                { "data": "description", "title": "Descripción", "visible": true, "orderable": true },
                {
                    "targets": -1,
                    "data": null,
                    "className": "",
                    "width": "110px",
                    "defaultContent": '<div class="text-center bt_dt">' +
                    '<button type="button" class="btn btn-sm default blue modal-toggle" id="btEditar" title="Modificar">' +
                    '<i class="fa fa-edit"></i>' +
                    '</button>' +
                    '<button type="button" class="btn btn-sm default red modal-toggle" id="btEliminar" title="Baja">' +
                    '<i class="fa fa-trash"></i>' +
                    '</button>' +
                    '</div>',
                    "orderable": false
                }
            ],
            columnDefs: [{
                /*orderable: false,
                className: 'select-checkbox',
                targets:   0*/
            }],
            select: "single",
            deferRender: true,
            buttons: [
                { extend: 'copy', className: 'btn red btn-outline' },
                { extend: 'excel', className: 'btn yellow btn-outline ' },
                { extend: 'csv', className: 'btn purple btn-outline ' },
                { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns' },
                {
                    text: 'Recargar',
                    className: 'btn default',
                    action: function (e, dt, node, config) {
                        dt.clearPipeline().draw();
                        //dt.ajax.reload();
                        //$('table.dataTable').DataTable().clearPipeline().draw();
                    }
                },
            ],
            responsive: false,
            "order": [
                /*[0, 'asc']*/
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, 60] // change per page values here
            ],
            "pageLength": 10,
        });

        //$("div.toolbar").html('<select id="dtF" name="dtF" class="form-control input-sm input-small input-inline" aria-controls="dtTable"><option value=""></option><option value="1">Activos</option><option value="0">Inactivos</option></select>');

        $('#dtF').change(function () {
            $('table.dataTable').DataTable().clearPipeline().draw();
            //table.draw();
        });
        /*setInterval( function () {
            oTable.DataTable().ajax.reload( null, false );
        }, 30000);*/

        $('#table_tools > li > a.tool-action').on('click', function () {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });

        $('#dtTable tbody').on('click', '#btEditar, #btEliminar', function () {
            var data = oTable.DataTable().row($(this).parents('tr')).data();
            //console.log(data);
            if (this.id == "btEditar") {

                editar(data);

            }
            if (this.id == "btEliminar") {
                bootbox.dialog({
                    message: "Está seguro de eliminar el registro seleccionado?",
                    title: "Confirmación",
                    buttons: {
                        yes: {
                            label: "Si",
                            className: "green",
                            callback: function () {
                                $.get($url + "/delete/" + data.id, function (response) {
                                    $('table.dataTable').DataTable().clearPipeline().draw();
                                });
                            }
                        },
                        no: {
                            label: "No",
                            className: "red",
                            callback: function () {
                                console.log("no");
                            }
                        }
                    }
                });

            }
        });

        $('#dtTable').on('click', 'tr', function(event) {
            //
        }).on('dblclick', 'td', function(event) {
            var data = oTable.DataTable().row(this).data();
            editar(data);
        });

        $('#dtTable tbody').on('click', 'td', function () {
            var cell_clicked    = oTable.DataTable().cell(this);
            console.log(cell_clicked.index().column);
            if(cell_clicked.index().column != 7){
                var data = oTable.DataTable().row($(this).parents('tr')).data();
                //leerMensaje(data);
                console.log(data);
            }

        });
    }

    $('#btGuardar').on('click', function (e) {
        guardar();
    });

    var editar = function(data){

        //$('.mCRUDTitle').html('Locatorio [' + data.idLocatario + ']');

        $.get($url + "/get/" + data.id, function (response) {
            PopulateForm("#formCRUD", response.data[0]);
            $('#mCRUD').modal('toggle');
        });
    }

    var guardar = function () {
        if (formCRUD.valid()) {

            var url = $url + '/save';

            $("#formCRUD").ajaxSubmit({
                url: url,
                type: 'post',
                beforeSubmit: function (formData, jqForm, options) {
                    console.log("beforeSubmit");
                },
                success: function (responseText, statusText, xhr, $form) {
                    $('table.dataTable').DataTable().clearPipeline().draw();
                    $('#mCRUD').modal('toggle');
                }
            });

        } else {
            console.log("No valid");
            return false;
        }
    }

    return {

        init: function () {

            initDataTable();
            setJQueryValidate();

        }
    };
}();

jQuery(document).ready(function () {
    permisos.init();

    $('#btnAgregar').on('click', function (e) {
        formReset($('#formCRUD'));
    });

    $('#btCancelar').on('click', function (e) {
        formReset($('#formCRUD'));
        $('#mCRUD').modal('toggle');
    });
});