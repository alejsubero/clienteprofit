var roles = function () {

    var formCRUD = $('#formCRUD');

    var setJQueryValidate = function(){
        //jQuery.validator.setDefaults({
        formCRUD.validate({
            doNotHideMessage: true,
            errorElement: 'span',
            errorClass: 'help-block help-block-error',
            focusInvalid: true,
            ignoreTitle: true,
            //ignore: '',
            messages : {
                required: "Este campo es requerido.",
                number : "Por favor ingrese solo números."
            },
            rules : {

            },
            invalidHandler : function (event, validator) {
                //$('.alert-error', $('.login-form')).show();
            },
            errorPlacement: function (error, element) {
                var cont = $(element).parent('.input-group');

                /* if (cont) {
                    console.log(cont);
                    cont.after(error);
                } else {
                    console.log(element);
                    //element.after(error);
                    error.appendTo(element.closest(".form-group"));
                }*/
                error.appendTo(element.closest(".form-group"));
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (element) {
                element.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {

            }
        });

        //formCRUD.validate();
    }

    var initDataTable = function(){

        var table = $('#dtTable');

        var oTable = table.dataTable({
            dom: '<"row view-filter"<"col-sm-8"<"pull-left"f><"pull-left"<"toolbar">>><"col-sm-4"<"text-right"p>>>t<"row view-pager"<"col-sm-6"<"text-left"i>><"col-sm-6"<"text-right"p>>>',//'ftip',
            "language": languageDataTablet,
            fixedHeader: {
                header: true,
                headerOffset: $('.page-header').outerHeight(true)
            },
            "loadingMessage": 'Cargando...',
            "processing": true,
            "serverSide": true,
            "ajax": $.fn.dataTable.pipeline({
                url:  $url + "/get",
                pages: 5 // number of pages to cache
            }),
            "createdRow": function ( row, data, index ) {
                //console.log(data.Hab);
                //if (data.Hab == 1) {

                    //$(row).addClass('');
                //}else{
                //    $(row).addClass('desh');
                //}
            },
            "columns": [
                { "data": "id", "title": "id", "visible": false, "orderable": true},
                { "data": "name", "title": "Nombre", "visible": true, "orderable": true},
                { "data": "slug", "title": "Abrev.", "visible": true, "orderable": true },
                { "data": "description", "title": "Descripción", "visible": true, "orderable": true },
                {
                    "targets": -1,
                    "data": null,
                    "className" : "",
                    "width": "110px",
                    "defaultContent": '<div class="text-center bt_dt">' +
                        '<button type="button" class="btn btn-sm default blue modal-toggle" id="btEditar" title="Modificar, agregar y quitar permisos">' +
                            '<i class="fa fa-edit"></i>' +
                        '</button>' +
                        '<button type="button" class="btn btn-sm default red modal-toggle" id="btEliminar" title="Baja">' +
                            '<i class="fa fa-trash"></i>' +
                        '</button>' +
                    '</div>',
                    "orderable": false
                }
            ],
            select: "single",
            deferRender: true,
            buttons: [
                { extend: 'copy', className: 'btn red btn-outline' },
                { extend: 'excel', className: 'btn yellow btn-outline ' },
                { extend: 'csv', className: 'btn purple btn-outline ' },
                { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'},
                {
                    text: 'Recargar',
                    className: 'btn default',
                    action: function ( e, dt, node, config ) {
                        dt.clearPipeline().draw();
                        //dt.ajax.reload();
                        //$('table.dataTable').DataTable().clearPipeline().draw();
                    }
                },
            ],
            responsive: false,
            "order": [
                /*[0, 'asc']*/
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, 60] // change per page values here
            ],
            "pageLength": 30,
        });

        //$("div.toolbar").html('<select id="dtF" name="dtF" class="form-control input-sm input-small input-inline" aria-controls="dtTable"><option value=""></option><option value="1">Activos</option><option value="0">Inactivos</option></select>');

        $('#dtF').change(function() {
            $('table.dataTable').DataTable().clearPipeline().draw();
            //table.draw();
        });
        /*setInterval( function () {
            oTable.DataTable().ajax.reload( null, false );
        }, 30000);*/

        $('#table_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });

        $('#dtTable tbody').on('click', '#btEditar, #btEliminar', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();
            //console.log(data);
            if(this.id == "btEditar"){

                editar(data);

            }
            if(this.id == "btEliminar"){
                bootbox.dialog({
                    message: "Está seguro de eliminar el registro seleccionado?",
                    title: "Confirmación",
                    buttons: {
                        yes: {
                            label: "Si",
                            className: "green",
                            callback: function() {
                                $.get($url + "/delete/"+data.id, function(response) {
                                    $('table.dataTable').DataTable().clearPipeline().draw();
                                });
                            }
                        },
                        no: {
                            label: "No",
                            className: "red",
                            callback: function() {
                                console.log("no");
                            }
                        }
                    }
                });

            }
        });

        $('#dtTable').on('click', 'tr', function(event) {
            //
        }).on('dblclick', 'td', function(event) {
            var data = oTable.DataTable().row(this).data();
            editar(data);
        });

        /*$('#dtTableEnviados tbody').on('click', 'td', function () {
            var cell_clicked    = oTable.DataTable().cell(this);
            if(cell_clicked.index().column != 7){
                var data = oTable.DataTable().row($(this).parents('tr')).data();
                leerMensaje(data);
            }

        });*/
    }

    var initDataTable2 = function(idRol){

        var table = $('#dtPermisos');

        var oTable = table.dataTable({
            "language": languageDataTablet,
            "loadingMessage": 'Cargando...',
            "ajax": {
                "url": $url + "/getPermisos/"+idRol,
                "dataSrc": "data"
            },
            "createdRow": function ( row, data, index ) {
                if (data.estado == 1) {

                }else{
                    $(row).addClass('read');
                }
            },
            "columns": [
                { "data": "id", "title": "Id", "visible": false },
                { "data": "idRol", "title": "idRol", "visible": false },
                {
                    data:   "id",
                    render: function ( data, type, row ) {
                        if ( type === 'display' ) {
                            //return '<input type="checkbox" class="select">';
                            return '<label class="mt-checkbox">' +
                                        '<input type="checkbox" id="chk" name="chk" class="select"' + row.chk + '>' +
                                        '<span></span>' +
                                    '</label>';
                        }
                        return data;
                    },
                    className: "dt-body-center"
                },
                { "data": "name", "title": "Nombre", "visible": true },
                { "data": "slug", "title": "Abrev.", "visible": true },
                { "data": "description", "title": "Descripción", "visible": true }
            ],
            rowCallback: function ( row, data ) {
                // Set the checked state of the checkbox in the table
                //$('input.editor-active', row).prop( 'checked', data.active == 1 );
                //console.log(row);
                //console.log(data);
            },
            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
            select: "single",
            //scrollY: "350vh",
            deferRender: true,
            //scrollCollapse: true,

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: false,
            "order": [
                /*[0, 'asc']*/
            ],

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
        });


        $('#dtPermisos tbody').on('click', '.select', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();
            console.log(data.idRol, data.id);
            url = "";
            if($(this).is(':checked')){
                url = $url + "/AddPermission";
            }else{
                url = $url + "/revokePermission";
            }
            $.post(url, { 'idRol' : data.idRol, 'idPermission' : data.id}, function(response) {
                //$('#dtTable').dataTable().api().ajax.reload();
            });
        });
    }

    $('.btGuardar').on('click', function (e) {
        var sufj = $(this).attr('data-form');
        guardar(sufj);
    });

    var editar = function(data){
        if($.fn.dataTable.isDataTable('#dtPermisos')){
            $('#dtPermisos').dataTable().api().ajax.url($url + "/getPermisos/"+data.id).load();
        }else{
            initDataTable2(data.id);
        }
        $.get($url + "/get/"+data.id, function(response) {
            PopulateForm("#formCRUDEdit", response.data[0]);
            $('#mCRUDEdit').modal('toggle');
        });
    }

    var guardar = function(sufj) {
        //var form = $("#formCRUD");

        if($('#formCRUD'+sufj).valid()){
            //var formJson = dojo.formToJson("formCRUD");
            //var formJson = dojo.formToObject("formCRUD");
            //var formJson = $('#formCRUD').serialize()

            //console.log(formJson);
            var url = $url + '/save';

            $('#formCRUD'+sufj).ajaxSubmit({
                url: url,
                type: 'post',
                beforeSubmit:function(formData, jqForm, options){
                    console.log("beforeSubmit");
                },
                success: function(responseText, statusText, xhr, $form){
                    $('table.dataTable').DataTable().clearPipeline().draw();
                    $('#mCRUD'+sufj).modal('toggle');
                }
            });
        }else{
            console.log("No valid");
            return false;
        }
    }

    return {

        init: function () {
            /*if (!jQuery().bootstrapWizard) {
                return;
            }*/


            initDataTable();
            //initDataTable2();
            setJQueryValidate();

        }
    };
}();

jQuery(document).ready(function() {
    roles.init();

    $('#btnAgregar').on('click', function (e) {
        formReset($('#formCRUD'));
    });

    $('#btCancelar').on('click', function (e) {
        formReset($('#formCRUD'));
        $('#mCRUD').modal('toggle');
    });

    $('#btCancelar2').on('click', function (e) {
        formReset($('#formCRUDEdit'));
        $('#mCRUDEdit').modal('toggle');
    });
});