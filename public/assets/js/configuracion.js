var configuracion = function () {

    var formCRUD = $('#formCRUD');

    var setJQueryValidate = function(){
        //jQuery.validator.setDefaults({
        formCRUD.validate({
            doNotHideMessage: true,
            errorElement: 'span',
            errorClass: 'help-block help-block-error',
            focusInvalid: true,
            ignoreTitle: true,
            //ignore: '',
            messages : {
                required: "Este campo es requerido.",
                number : "Por favor ingrese solo números."
            },
            rules : {

            },
            invalidHandler : function (event, validator) {
                //$('.alert-error', $('.login-form')).show();
            },
            errorPlacement: function (error, element) {
                var cont = $(element).parent('.input-group');

                /* if (cont) {
                    console.log(cont);
                    cont.after(error);
                } else {
                    console.log(element);
                    //element.after(error);
                    error.appendTo(element.closest(".form-group"));
                }*/
                error.appendTo(element.closest(".form-group"));
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (element) {
                element.closest('.form-group').removeClass('has-error');
            },
            submitHandler: function (form) {

            }
        });

        //formCRUD.validate();
    }

     var initDataTable = function(){

        var table = $('#dtTable');

        var oTable = table.dataTable({
            //destroy:true,
            "language": languageDataTablet,
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  $url + "/getClientes",// ajax source
                "dataSrc": "list"
            },
            "createdRow": function ( row, data, index ) {
                if (data.estado == 1) {
                    //$(row).addClass('un-read');
                }else{
                    $(row).addClass('read');
                }
            },
            "columns": [

                { "data": "razon_social", "title": "Nombre Empresa", "visible": true},
                { "data": "email", "title": "Email", "visible":true},
                { "data": "direccion_entrega", "title": "Dirección de Entrega", "visible": true},
                { "data": "contacto", "title": "Persona Contacto", "visible": true},
                { "data": "telefono", "title": "Teléfonos", "visible": true},
        {
            "targets": -1,
            "data": null,
            "className" : "",
            "width": "110px",
            "defaultContent": '<div class="text-center bt_dt">' +
            '<button type="button" class="btn btn-sm default blue modal-toggle" id="btEditar" title="Modificar">' +
            '<i class="fa fa-edit"></i>' +
            '</button>' +
            '<button type="button" class="btn btn-sm default red modal-toggle" id="btEliminar" title="Baja">' +
            '<i class="fa fa-close"></i>' +
            '</button>' +
            '</div>',
            "orderable": false
        }
        ],
        columnDefs: [ {
        }],
        select: "single",
        buttons: [
        { extend: 'copy', className: 'btn red btn-outline' },
        { extend: 'excel', className: 'btn yellow btn-outline ' },
        { extend: 'csv', className: 'btn purple btn-outline ' },
        { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'},
        {
            text: 'Recargar',
            className: 'btn default',
            action: function ( e, dt, node, config ) {
                dt.clearPipeline().draw();
            }
        },
        ],
        responsive: false,
        "order": [
        ],
        "lengthMenu": [
        [5, 10, 15, 20, -1],
                [5, 10, 15, 20, 60] // change per page values here
                ],
                "pageLength": 30,
            });


        $('#table_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });

        $('#dtTable tbody').on('click', '#btEditar, #btEliminar', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();
            if(this.id == "btEditar"){

                editar(data);

            }
            if(this.id == "btEliminar"){
                bootbox.dialog({
                    message: "¿Está seguro de eliminar el registro seleccionado?",
                    title: "Confirmación",
                    buttons: {
                        yes: {
                            label: "Sí",
                            className: "green",
                            callback: function() {
                                $.get($url + "/delete/"+data.id, function(response) {
                                    if(response.success==true && response.id==-1)
                                    {
                                        toastr.options = {
                                            "newestOnTop": true,
                                            "positionClass": "toast-bottom-right",
                                        }
                                        toastr.warning('Esta categoría se encuentra asociada a una pieza. Elimine la pieza y luego podrá eliminar la categoría.', 'Información');
                                    }else if(response.success==true && response.id==data.id)
                                    {
                                        $('table.dataTable').DataTable().clearPipeline().draw();
                                        toastr.options = {
                                            "newestOnTop": true,
                                            "positionClass": "toast-bottom-right",
                                        }
                                        toastr.success('Registro eliminado', 'Información');
                                    }
                                });
                            }
                        },
                        no: {
                            label: "No",
                            className: "red",
                            callback: function() {
                                console.log("no");
                            }
                        }
                    }
                });
            }
        });

        $('#dtTable').on('click', 'tr', function(event) {
            //
        }).on('dblclick', 'td', function(event) {
            var data = oTable.DataTable().row(this).data();
            editar(data);
        });
    }



    $('#btnConfigUser').on('click', function (e) {
   
        guardar();

    });

    var editar = function(data){
        if($.fn.dataTable.isDataTable('#dtPermisos')){
            $('#dtPermisos').dataTable().api().ajax.url($url + "/getPermisos/"+data.id).load();
        }else{
            initDataTable2(data.id);
        }
        $.get($url + "/get/"+data.id, function(response) {
            PopulateForm("#formCRUDEdit", response.data[0]);
            $('#mCRUDEdit').modal('toggle');
        });
    }


    var guardar = function () {
        if (formCRUD.valid()) {
            var id = $("#id").val();
            var url = $url + '/save';
            $("#formCRUD").ajaxSubmit({
                url: url,
                type: 'post',
                beforeSubmit: function (formData, jqForm, options) {
                },
                success: function (responseText, statusText, xhr, $form) {
                    if(responseText.success){
                        bootbox.alert({
                        size: "",
                        title: "Guardar Registro",
                        message: "Registro Procesado con Exito!!!",
                        callback: function () {
                          //  $('#rawmaterial_id').val('null').trigger('change');
                          $('#usuario_id,#vendedor,#zona_id,#tipo_cliente_id,#segmento_id').val('null').trigger('change');

                        }
                    });

                    }else{

                    }
                          
                   
                }
            });

        } else {
            console.log("No valid");
            return false;
        }
    }

    return {

        init: function () {
            //initDataTable();
            setJQueryValidate();
            fillComboBox2($urlBase + '/usuarios/getComboUsuarios', $("#usuario_id"));

        }
    };
}();

jQuery(document).ready(function() {
    configuracion.init();

    $('#btnAgregar').on('click', function (e) {
        formReset($('#formCRUD'));
    });

    $('#btCancelar').on('click', function (e) {
        formReset($('#formCRUD'));
        $('#mCRUD').modal('toggle');
    });

    $('#btCancelar2').on('click', function (e) {
        formReset($('#formCRUDEdit'));
        $('#mCRUDEdit').modal('toggle');
    });
});